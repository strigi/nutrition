package be.rivendale.nutrition;

import com.google.common.math.DoubleMath;

public class MathUtilities {
    private static final double EPSILON = 0.001;

    /**
     * Checks if two doubles are close enough to equal.
     * @param a A double to check for equality.
     * @param b The other double to check for equality.
     * @return True if they differ by at most one thousandth.
     */
    public static boolean doubleEquals(double a, double b) {
        return DoubleMath.fuzzyEquals(a, b, EPSILON);
    }
}
