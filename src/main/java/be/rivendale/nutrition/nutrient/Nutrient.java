package be.rivendale.nutrition.nutrient;

import be.rivendale.nutrition.domain.Identifiable;
import be.rivendale.nutrition.domain.NutrientException;
import org.codehaus.jackson.annotate.JsonTypeInfo;

import javax.persistence.*;
import java.util.Map;

@Entity
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
public abstract class Nutrient implements Identifiable {
    protected static final double CALORIES_PER_JOULE = 0.239005736;

    @Id
    @GeneratedValue
    private Integer id;

    /**
     * The name of the nutrient.
     */
    private String name;

    @Lob
    private String comments;

    @Embedded
    private Serving serving;

    /**
     * Used by JPA for direct query. The field is redundant.
     * TODO: replace this with formula query?
     */
    private double energy;

    /**
     * Used by JPA.
     */
    protected Nutrient() {
    }

    /**
     * Constructor for facilitating subclasses.
     * @param name The name of the nutrient. Mandatory.
     * @param serving The serving size of this nutrient. Optional.
     * @param energy The
     */
    protected Nutrient(String name, Serving serving, double energy, String comments) {
        this.comments = comments;
        assignName(name);
        this.energy = energy;
        this.name = name;
        this.serving = serving;
    }

    private void assignName(String name) {
        if(name == null || name.trim().isEmpty()) {
            throw new NutrientException("Nutrient name is mandatory");
        }
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getComments() {
        return comments;
    }

    public Serving getServing() {
        return serving;
    }

    public double getEnergyAsSmallCalories() {
        return getEnergy() * CALORIES_PER_JOULE;
    }

    /**
     * Returns the number of joules per gram.
     * This is the same as a ratio between 0..1.
     */
    public double getEnergy() {
        return energy;
    }

    /**
     * Returns a map of each macronutrient component and it's value.
     * Values are represented per gram.
     * @return The map of macronutrients.
     */
    public abstract Map<NutrientComponent, Double> getComponents();
}
