package be.rivendale.nutrition.nutrient;

import be.rivendale.nutrition.domain.Ingredient;

import java.util.Collection;

/**
 * Represents a macronutrient component.
 */
public enum NutrientComponent {
    /**
     * Represents the amount of joules per gram.
     */
    energy,

    /**
     * Represents the amount of proteins mass per gram (in grams, thus ranging from 0..1).
     */
    proteins,

    /**
     * Represents the amount of carbohydrates mass per gram (in grams, thus ranging from 0..1).
     */
    carbs,

    /**
     * Represents the amount of fats mass per gram (in grams, thus ranging from 0..1).
     */
    fats,

    /**
     * Represents the amount of fruit mass per gram (in grams, thus ranging from 0..1).
     */
    fruits,

    /**
     * Represents the amount of vegetable mass per gram (in grams, thus ranging from 0..1).
     */
    vegetables;

    /**
     * Creates the sum of all ingredients for this component.
     * @return The sum of the specified macronutrient component for all ingredients.
     */
    public double sum(Collection<Ingredient> ingredients) {
        return ingredients.stream().mapToDouble(i -> i.quantity(this)).sum();
    }
}
