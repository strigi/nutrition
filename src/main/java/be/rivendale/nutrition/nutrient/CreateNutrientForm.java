package be.rivendale.nutrition.nutrient;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateNutrientForm {
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class IngredientForm {
        public Double weight;
        public Integer id;
    }

    public static class Components {
        public Double energy;
        public Double proteins;
        public Double carbs;
        public Double fats;
        public Double fruits;
        public Double vegetables;
    }

    public String name;
    public String type;
    public String comments;

    public List<IngredientForm> ingredients = new ArrayList<>();
    public Components components = new Components();
}
