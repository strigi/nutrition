package be.rivendale.nutrition.nutrient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Collection;

@Embeddable
public class Serving {
    private static final double NUMBER_OF_MEASUREMENTS_FOR_ACCURATE_APPROXIMATION = 10;

    @Transient
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private String label;

    @ElementCollection(fetch = FetchType.EAGER)
    private Collection<Double> measurements = new ArrayList<>();

    /**
     * Used by JPA.
     */
    Serving() {
    }

    /**
     * Creates a new serving.
     * @param label The name of this serving
     * @param measurements The measurement samples in grams that should be averaged to obtain a weight approximation.
     */
    public Serving(String label, double... measurements) {
        this.label = label;
        assignMeasurements(measurements);
    }

    private void assignMeasurements(double[] measurements) {
        if(measurements == null || measurements.length == 0) {
            throw new IllegalArgumentException("At least one measurement must be provided.");
        } else if(measurements.length < NUMBER_OF_MEASUREMENTS_FOR_ACCURATE_APPROXIMATION) {
            logger.warn("Serving '" + label + "' has less than " + NUMBER_OF_MEASUREMENTS_FOR_ACCURATE_APPROXIMATION + " measurements. Fix this to increase estimation accuracy.");
        }
        for (double measurement : measurements) {
            this.measurements.add(measurement);
        }
    }

    public String getLabel() {
        return label;
    }

    public double getWeight() {
        double total = 0;
        for (double measurement : measurements) {
            total += measurement;
        }
        return total / measurements.size();
    }
}
