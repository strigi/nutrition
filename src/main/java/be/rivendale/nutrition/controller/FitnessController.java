package be.rivendale.nutrition.controller;

import be.rivendale.nutrition.nutrient.Nutrient;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Map;

@Controller
public class FitnessController {
    @PersistenceContext
    private EntityManager entityManager;

    @RequestMapping("/fitness")
    public String meal(Map<String, Object> model) {
        model.put("nutrients", entityManager.createQuery("select n from Nutrient n", Nutrient.class).getResultList());
        return "fitness";
    }
}
