package be.rivendale.nutrition.controller;

import org.springframework.context.expression.MapAccessor;
import org.springframework.expression.Expression;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
public class ExperimentController {
    private static final SpelExpressionParser EXPRESSION_PARSER = new SpelExpressionParser();

    @RequestMapping(method = RequestMethod.POST, value = "experiment")
    public @ResponseBody String experiment(@RequestBody Map<String, Object> user) {
        return "ok: " + get(user, "address.city.postalCode", String.class);
    }

    private <T> T get(Map<String, Object> map, String expressionString, Class<T> clazz) {
        Expression expression = EXPRESSION_PARSER.parseExpression(expressionString);
        StandardEvaluationContext evaluationContext = new StandardEvaluationContext(map);
        evaluationContext.addPropertyAccessor(new MapAccessor());
        return expression.getValue(evaluationContext, clazz);
    }
}
