package be.rivendale.nutrition.day;

import be.rivendale.nutrition.domain.Identifiable;
import be.rivendale.nutrition.domain.Ingredient;
import be.rivendale.nutrition.nutrient.NutrientComponent;

import javax.persistence.*;
import java.util.*;

import static be.rivendale.nutrition.nutrient.NutrientComponent.*;
import static javax.persistence.CascadeType.PERSIST;

@Entity
public class Day implements Identifiable {
    @Id
    @GeneratedValue
    private Integer id;

    @Temporal(TemporalType.DATE)
    private Date date;

    @OneToMany(cascade = PERSIST)
    @OrderColumn
    private List<Ingredient> entries = new LinkedList<>();

    /**
     * Used by JPA.
     */
    protected Day() {
    }

    public Day(Date date) {
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public Date getDate() {
        return this.date;
    }

    public Map<NutrientComponent, Double> getCurrent() {
        Map<NutrientComponent, Double> values = new HashMap<>();
        for (NutrientComponent component : NutrientComponent.values()) {
            values.put(component, component.sum(entries));
        }
        return Collections.unmodifiableMap(values);
    }

    /**
     * TODO: These values are precalculated and hardcoded.
     */
    public Map<NutrientComponent, Double> getTarget() {
        Map<NutrientComponent, Double> values = new HashMap<>();
        values.put(proteins, 178.0);
        values.put(carbs, 206.0);
        values.put(fats, 107.0);
        values.put(energy, 2492.0 * 4184.0);
        values.put(fruits, 2 * 125.0);
        values.put(vegetables, 3 * 125.0);
        return Collections.unmodifiableMap(values);
    }

    public Map<NutrientComponent, Double> getRemaining() {
        Map<NutrientComponent, Double> values = new HashMap<>();
        for (NutrientComponent component : values()) {
            values.put(component, getTarget().get(component) - getCurrent().get(component));
        }
        return Collections.unmodifiableMap(values);
    }

    public void addEntry(Ingredient ingredient) {
        entries.add(0, ingredient);
    }

    public List<Ingredient> getEntries() {
        return Collections.unmodifiableList(entries);
    }
}
