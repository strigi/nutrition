package be.rivendale.nutrition.configuration.autoupgrade;

public class AutoUpgradeException extends RuntimeException {
    public AutoUpgradeException(String format, Object... arguments) {
        this(null, format, arguments);
    }

    public AutoUpgradeException(Throwable cause, String format, Object... arguments) {
        super(String.format(format, arguments), cause);
    }
}
