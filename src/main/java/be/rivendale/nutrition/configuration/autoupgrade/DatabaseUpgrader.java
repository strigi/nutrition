package be.rivendale.nutrition.configuration.autoupgrade;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Stream;

public class DatabaseUpgrader {
    private static final String VERSION_TABLE_NAME = "database_version_history";
    private static final String EXECUTED_SCRIPTS_COLUMN_NAME = "executed_scripts";
    private static final String SCRIPTS_LOCATION_PATTERN = "classpath:/scripts/*.sql";

    private Logger logger = LoggerFactory.getLogger(getClass());

    private Connection connection;

    public DatabaseUpgrader(Connection connection) {
        this.connection = connection;
    }

    public void upgrade() throws AutoUpgradeException {
        logger.info("Upgrading database to the latest version");
        scriptsToExecute().forEach(this::executeScript);
    }

    private void executeScript(Resource resource) {
        try {
            logger.info("Executing script '" + resource.getFilename() + "'");
            executeStatementsInScript(Arrays.stream(Files.toString(resource.getFile(), Charsets.UTF_8).split(";")).map(String::trim));
            registerExecutedScript(resource);
        } catch (SQLException | IOException exception) {
            throw new AutoUpgradeException(exception, "Unable to execute script '%s'", resource.getFilename());
        }
    }

    private void executeStatementsInScript(Stream<String> statements) throws IOException {
        statements.forEach(statement -> {
            logger.info("Executing statement:\n" + statement);
            executeStatement(statement);
        });
    }

    private void registerExecutedScript(Resource resource) throws SQLException {
        executeStatement("insert into %s(%s) values('%s')", VERSION_TABLE_NAME, EXECUTED_SCRIPTS_COLUMN_NAME, resource.getFilename());
    }

    private ResultSet executeStatement(String sqlStatement, Object... arguments) {
        String formattedStatement = String.format(sqlStatement, arguments);
        try {
            Statement statement = connection.createStatement();
            statement.execute(formattedStatement);
            return statement.getResultSet();
        } catch (SQLException exception) {
            throw new AutoUpgradeException(exception, "Unable to execute statement: '%s'", formattedStatement);
        }
    }

    private Stream<Resource> scriptsToExecute() {
        try {
            createVersionTableIfNecessary();
            Collection<String> alreadyExecutedScripts = retrieveAlreadyExecutedScripts();
            return bundledScripts().filter(resource -> !alreadyExecutedScripts.contains(resource.getFilename()));
        } catch (SQLException | IOException exception) {
            throw new AutoUpgradeException(exception, "Unable to determine scripts to execute");
        }
    }

    private Stream<Resource> bundledScripts() throws IOException {
        return Arrays.stream(new PathMatchingResourcePatternResolver().getResources(SCRIPTS_LOCATION_PATTERN)).sorted((a, b) -> a.getFilename().compareTo(b.getFilename()));
    }

    private Collection<String> retrieveAlreadyExecutedScripts() throws SQLException {
        ResultSet resultSet = executeStatement("select * from %s order by 1", VERSION_TABLE_NAME);
        ArrayList<String> scripts = new ArrayList<>();
        while(resultSet.next()) {
            scripts.add(resultSet.getString(EXECUTED_SCRIPTS_COLUMN_NAME));
        }
        return scripts;
    }

    private void createVersionTableIfNecessary() throws SQLException {
        executeStatement("create table if not exists %s(%s varchar(5000) not null)", VERSION_TABLE_NAME, EXECUTED_SCRIPTS_COLUMN_NAME);
    }
}
