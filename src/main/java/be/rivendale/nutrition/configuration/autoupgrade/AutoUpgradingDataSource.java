package be.rivendale.nutrition.configuration.autoupgrade;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;

public class AutoUpgradingDataSource extends DriverManagerDataSource {
    private static boolean isUpgraded = false;

    public AutoUpgradingDataSource(Class<? extends Driver> driverClass, String url, String username, String password) {
        super(url, username, password);
        setDriverClassName(driverClass.getName());
    }

    @Override
    public Connection getConnection() throws SQLException {
        if(!isUpgraded) {
            upgradeDatabase();
            isUpgraded = true;
        }
        return super.getConnection();
    }

    private void upgradeDatabase() throws SQLException {
        try(Connection connection = super.getConnection()) {
            connection.setAutoCommit(false);
            new DatabaseUpgrader(connection).upgrade();
            connection.commit();
        }
    }
}
