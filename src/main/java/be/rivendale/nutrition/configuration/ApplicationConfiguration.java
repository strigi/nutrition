package be.rivendale.nutrition.configuration;

import be.rivendale.nutrition.configuration.autoupgrade.AutoUpgradingDataSource;
import com.mysql.jdbc.Driver;
import org.hibernate.dialect.MySQL5Dialect;
import org.hibernate.ejb.HibernatePersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.Arrays;
import java.util.HashMap;

import static be.rivendale.nutrition.configuration.ApplicationConfiguration.ROOT_PACKAGE_NAME;
import static org.springframework.context.annotation.ComponentScan.Filter;

@Configuration
@ComponentScan(value = ROOT_PACKAGE_NAME, excludeFilters = @Filter(Configuration.class))
@EnableTransactionManagement
public class ApplicationConfiguration {
    private Logger logger = LoggerFactory.getLogger(getClass());

    static final String ROOT_PACKAGE_NAME = "be.rivendale.nutrition";

    @Autowired
    private Environment environment;

    private Profile profile;

    @PostConstruct
    public void initialize() {
        setupProfile();
        printLogo();
    }

    private void setupProfile() {
        String[] profiles = environment.getActiveProfiles();
        if(profiles.length != 1) {
            throw new RuntimeException("Exactly one profile must be active. Currently the active profiles are '" + Arrays.toString(profiles) + "'");
        }
        profile = Profile.valueOf(profiles[0]);
        logger.info("Starting application with profile '" + profile + "'");
    }

    private void printLogo() {
        logger.info("Rivendale presents:");
        logger.info("  _   _         _          _  _    _               ");
        logger.info(" | \\ | |       | |        (_)| |  (_)              ");
        logger.info(" |  \\| | _   _ | |_  _ __  _ | |_  _   ___   _ __  ");
        logger.info(" | . ` || | | || __|| '__|| || __|| | / _ \\ | '_ \\ ");
        logger.info(" | |\\  || |_| || |_ | |   | || |_ | || (_) || | | |");
        logger.info(" |_| \\_| \\__,_| \\__||_|   |_| \\__||_| \\___/ |_| |_|");
        logger.info(" Profile: '" + profile + "'");
    }

    @Bean
    public DataSource dataSource() {
        return new AutoUpgradingDataSource(Driver.class, "jdbc:mysql://localhost:3306/" + profile.databaseName(), "root", "root");
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setPersistenceUnitName(profile.databaseName());
        return transactionManager;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setPersistenceProvider(new HibernatePersistence());
        entityManagerFactoryBean.setPersistenceUnitName(profile.databaseName());
        entityManagerFactoryBean.setPackagesToScan(ROOT_PACKAGE_NAME);
        entityManagerFactoryBean.setJpaPropertyMap(jpaProperties());
        entityManagerFactoryBean.setDataSource(dataSource());
        return entityManagerFactoryBean;
    }

    private HashMap<String, Object> jpaProperties() {
        HashMap<String, Object> properties = new HashMap<>();
//        properties.put("hibernate.show_sql", true);
//        properties.put("hibernate.format_sql", true);
        properties.put("hibernate.dialect", MySQL5Dialect.class.getName());
        return properties;
    }
}
