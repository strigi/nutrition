package be.rivendale.nutrition.domain;

public class NutrientException extends RuntimeException {
    public NutrientException(String message) {
        super(message);
    }
}
