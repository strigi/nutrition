package be.rivendale.nutrition;

import java.time.LocalDate;

/**
 * Date proxy utility to allow for unit testing of 'now' dates.
 */
public class DateUtilities {
    public static LocalDate simulatedDate = null;

    public static LocalDate now() {
        if(simulatedDate == null) {
            return LocalDate.now();
        } else {
            return simulatedDate;
        }
    }
}
