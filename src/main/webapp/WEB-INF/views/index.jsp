<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Nutrition</title>

        <meta name="viewport" content="width=device-width"/>

        <link rel="stylesheet" type="text/css" href="/resources/style.css"/>
        <style type="text/css">
            dl {
                margin-bottom: 2rem;
            }

            dd {
                margin: 0.25rem 0 1rem 1rem;
            }

            dd > strong {
                color: red;
            }
        </style>
    </head>
    <body>
        <header>
            <h1>Nutrition home</h1>
            <p>This is the 'Nutrition' food tracking application.</p>
        </header>
        <main>
            <section class="card medium">
                <header>Nutrient tracking</header>
                <dl>
                    <dt>
                        <c:url value="/nutrient/list" var="nutrientListUrl"/>
                        <a href="${nutrientListUrl}">Nutrient list</a>
                    </dt>
                    <dd>
                        Static page showing a listing of all nutrients in the database. Clickable to detail page.
                        Not very nice looking and contains some formatting issues but very practical.
                    </dd>
                    <dt>
                        <c:url value="/day/list" var="daysUrl"/>
                        <a href="${daysUrl}">Day logger</a>
                    </dt>
                    <dd>
                        Shows an overview of all days that have registered values.
                    </dd>
                </dl>
            </section>
            <section class="card medium">
                <header>Old stuff</header>
                <dl>
                    <dt>
                        <c:url value="/fitness" var="fitnessFoodCalculatorUrl"/>
                        <a href="${fitnessFoodCalculatorUrl}">Fitness food calculator</a>
                    </dt>
                    <dd>
                        The original fitness food calculator. Allows you to specify goals personal characteristics to help
                        determine how to compose your meals.<br/>
                        <strong>Has some hard coded values! Does not work for women and people who are not 176cm long and 30 years of age.</strong>
                    </dd>

                    <dt>
                        <c:url value="/composer" var="mealComposerUrl"/>
                        <a href="${mealComposerUrl}">Meal composer</a>
                    </dt>
                    <dd>
                        Drag and drop based meal composer. Works for everyone, but does not calculate daily calorie needs.
                        Clickable to detail page.
                    </dd>
                </dl>
            </section>
        </main>
    </body>
</html>
