<html>
    <head>
        <title>List nutrients</title>

        <meta name="viewport" content="width=device-width"/>

        <link rel="stylesheet" type="text/css" href="/resources/style.css"/>

        <style type="text/css">
            .button {
                background-color: #71a7d6;
                text-decoration: none;
                font-style: italic;
                font-weight: bold;
                display: inline-block;
                padding: 1rem;
                border-radius: 0.20rem;
                color: #ffffff;
                margin-bottom: 1rem;
            }
        </style>

        <script type="application/javascript" src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.14/angular.js"></script>

        <script type="application/javascript">
            var nutrition = angular.module("nutrition", []);

            nutrition.controller('NutrientController', ['$scope', '$http', function($scope, $http) {
                $http.get('/nutrients/get').success(function(data) {
                    $scope.nutrients = data;
                });

                $scope.openNutrientDetails = function(nutrientId) {
                    window.open('/nutrient/' + nutrientId, '_self');
                }
            }]);
        </script>
    </head>
    <body ng-app="nutrition" ng-controller="NutrientController">
        <header>
            <h1>List nutrients</h1>
            <p>This page shows a list of all nutrients.</p>
        </header>
        <main>
            <section class="card large">
                <header>Nutrients</header>
                <a class="button" href="/nutrient/create">Create new nutrient</a>
                <table>
                    <tr>
                        <th>Name</th>
                        <th>Proteins (per 100g)</th>
                        <th>Carbs (per 100g)</th>
                        <th>Fats (per 100g)</th>
                        <th>Energy (per 100g)</th>
                    </tr>
                    <tr ng-repeat="nutrient in nutrients" ng-click="openNutrientDetails(nutrient.id)" class="selectable">
                        <td>{{nutrient.name}}</td>
                        <td class="numeric">{{nutrient.components['proteins'] * 100 | number: 2}}g</td>
                        <td class="numeric">{{nutrient.components['carbs'] * 100 | number: 2}}g</td>
                        <td class="numeric">{{nutrient.components['fats'] * 100 | number: 2}}g</td>
                        <td class="numeric">{{nutrient.components['energy'] / 10 | number: 0}}kJ</td>
                    </tr>
                </table>
            </section>
        </main>
    </body>
</html>
