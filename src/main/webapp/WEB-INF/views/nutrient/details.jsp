<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="n" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>

<html>
    <head>
        <title>Nutrient</title>

        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.css"/>
        <style type="text/css">
            @import url(http://fonts.googleapis.com/css?family=Exo+2);

            body {
                background-color: lightgray;
            }

            .content {
                width: 60rem;
                margin-left: auto;
                margin-right: auto;

                font-family: 'Exo 2', sans-serif;
            }

            .content > header {
                margin: 1rem;
                font-size: 3rem;
            }

            .cards-container {
                display: flex;
                flex-flow: wrap row;
            }

            .card {
                box-sizing: border-box;

                width: 30rem;
                height: 20rem;

                display: flex;

                color: rgb(25%, 25%, 25%);
            }

            .card-border {
                background: linear-gradient(30deg, #dddddd, #ffffff, #eeeeee);

                border: 0.1rem solid rgb(25%, 25%, 25%);

                margin: 1rem;
                padding: 1rem;

                flex-grow: 1;

                display: flex;
                flex-direction: column;

                box-shadow: 0.2rem 0.2rem 0.5rem rgba(0, 0, 0, 0.5);
            }

            .card-border > header {
                font-size: 1.5rem;
                margin-bottom: 1rem;
            }

            .card-border > main {
                flex-grow: 1;
            }
        </style>

        <style type="text/css">
            .macronutrients {
                display: flex;
                flex-direction: column;
            }

            .macronutrients > p {
                margin-bottom: 1rem;
            }

            .macronutrients > section:first-of-type {
                display: flex;
                font-size: 1.25rem;
                justify-content: space-between;
                margin-bottom: 1rem;
            }

            .macronutrients > section:last-of-type {
                flex-grow: 1;
            }
        </style>

        <style type="text/css">
            .energy {
                display: flex;
                flex-direction: column;
            }

            .energy > section:first-of-type {
                display: flex;
                justify-content: center;
                align-items: center;
                flex-grow: 1;
            }

            .energy > section:last-of-type {
                height: 4rem;
            }

            .energy-value {
                display: flex;
                flex-direction: column;
                align-items: flex-end;
            }

            .energy-value > div:first-of-type {
                font-size: 3rem;
            }

            .energy-value > div:last-of-type {
                font-size: 1.5rem;
            }
        </style>

        <style type="text/css">
            .fruit-and-vegetables {
                display: flex;
                flex-direction: column;
            }

            .fruit-and-vegetables > p {
                margin-bottom: 1rem;
            }

            .fruit-and-vegetables > section {
                flex-grow: 1;
            }
        </style>

        <style type="text/css">
            .comments {
                white-space: pre-wrap;
                overflow-y: scroll;
            }
        </style>

        <style type="text/css">
            .ingredients {
                overflow-y: scroll;
            }

            .ingredients > p {
                margin-bottom: 1rem;
            }

            .ingredients-table {
                width: 100%;
            }

            .ingredients-table td{
                padding: 0.25rem;
            }
        </style>

        <script type="application/javascript" src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.14/angular.js"></script>
        <script type="application/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.js"></script>
        <script type="application/javascript" src="//www.google.com/jsapi"></script>

        <script type="application/javascript">
            google.load('visualization', '1', {
                packages: ['corechart']
            });

            google.setOnLoadCallback(function() {
                angular.bootstrap(document.body, ['nutrition']);
            });

            var nutrition = angular.module("nutrition", []);

            nutrition.controller('NutritionController', ['$scope', '$http', '$filter', function($scope, $http, $filter) {
                $scope.extractNutrientIdFromUrl = function() {
                    var pathSegments = location.toString().split("/");
                    return pathSegments[pathSegments.length - 1];
                };

                $scope.initializeMacronutrientsCard = function(nutrient) {
                    var proteinsPer100Grams = nutrient.components.proteins * 100;
                    var carbsPer100Grams = nutrient.components.carbs * 100;
                    var fatsPer100Grams = nutrient.components.fats * 100;

                    var chartData = new google.visualization.DataTable();
                    chartData.addColumn('string');
                    chartData.addColumn('number', 'Weight');
                    chartData.addRows([
                        ['Proteins ', {v: proteinsPer100Grams, f: $filter('number')(proteinsPer100Grams, 2) + 'g'}],
                        ['Carbs', {v: carbsPer100Grams, f: $filter('number')(carbsPer100Grams, 2) + 'g'}],
                        ['Fats', {v: fatsPer100Grams, f: $filter('number')(fatsPer100Grams, 2) + 'g'}]
                    ]);

                    $scope.macronutrients = {
                        proteins: proteinsPer100Grams,
                        carbs: carbsPer100Grams,
                        fats: fatsPer100Grams,
                        chartData: chartData
                    };

                    var chartElement = $('#macronutrientsChart');
                    new google.visualization.BarChart(chartElement.get(0)).draw($scope.macronutrients.chartData, {
                        width: chartElement.width(),
                        height: chartElement.height(),
                        legend: { position: "none" },
                        fontSize: parseInt(chartElement.css('font-size')),
                        fontName: chartElement.css('font-family'),
                        backgroundColor: {
                            fill:'transparent'
                        },
                        hAxis: {maxValue: 100 },
                        chartArea: {
                            left: 4 * parseInt(chartElement.css('font-size'))
//                            width: "100%",
//                            height: "100%"
                        }
                    });
                };

                $scope.initializeEnergyCard = function(nutrient) {
                    var moreThan = nutrient.moreEnergyThanRatio * 100;
                    var lessThan = (1 - nutrient.moreEnergyThanRatio) * 100;

                    var chartData = new google.visualization.DataTable();
                    chartData.addColumn('string');
                    chartData.addColumn('number', 'Percentage');
                    chartData.addColumn('number', 'Percentage');
                    chartData.addRows([
                        [
                            'Energy',
                            {v: moreThan, f: 'More than ' + $filter('number')(moreThan, 0) + '% of all nutrients.'},
                            {v: lessThan, f: 'Less than ' + $filter('number')(lessThan, 0) + '% of all nutrients.'}
                        ]
                    ]);

                    $scope.energy = {
                        kiloJoules: nutrient.energy / 10,
                        kiloCalories: nutrient.energyAsSmallCalories / 10,
                        chartData: chartData
                    };

                    var chartElement = $('#energyChart');
                    new google.visualization.BarChart(chartElement.get(0)).draw($scope.energy.chartData, {
                        width: chartElement.width(),
                        height: chartElement.height(),
                        legend: { position: "none" },
                        fontSize: parseInt(chartElement.css('font-size')),
                        fontName: chartElement.css('font-family'),
                        backgroundColor: {
                            fill:'transparent'
                        },
                        hAxis: {maxValue: 100},
                        isStacked: true,
                        chartArea: {
                            left: 0,
                            top: 0,
                            width: "100%"
//                            height: "100%"
                        },
                        colors: ['green', 'red']
                    });
                };

                $scope.initializeFruitsAndVegetablesCard = function(nutrient) {
                    var fruitsPercentage = nutrient.components.fruits * 100;
                    var vegetablesPercentage = nutrient.components.vegetables * 100;

                    var chartData = new google.visualization.DataTable();
                    chartData.addColumn({type: 'string'});
                    chartData.addColumn({type: 'number', label: 'Percentage'});
                    chartData.addColumn({type: 'string', role: 'style'});
                    chartData.addRows([
                        [
                            'Fruits',
                            {v: fruitsPercentage , f: $filter('number')(fruitsPercentage, 0) + '%'},
                            'color: orange'

                        ],
                        [
                            'Vegetables',
                            {v: vegetablesPercentage , f: $filter('number')(vegetablesPercentage, 0) + '%'},
                            'color: green'

                        ]
                    ]);

                    $scope.fruitsAndVegetables = {
                        fruitsPercentage: fruitsPercentage,
                        vegetablesPercentage: vegetablesPercentage,
                        chartData: chartData
                    };

                    var chartElement = $('#fruitsAndVegetablesChart');
                    var chart = new google.visualization.ColumnChart(chartElement.get(0)).draw($scope.fruitsAndVegetables.chartData, {
                        width: chartElement.width(),
                        height: chartElement.height(),
                        legend: { position: "none" },
                        fontSize: parseInt(chartElement.css('font-size')),
                        fontName: chartElement.css('font-family'),
                        backgroundColor: {
                            fill:'transparent'
                        },
                        vAxis: {maxValue: 100 },
                        chartArea: {
//                            left: 4 * parseInt(chartElement.css('font-size')),
                        }
                    });
                };

                $scope.initializeCommentsCard = function(nutrient) {
                    $scope.comments = nutrient.comments;
                };

                $scope.initializeIngredientsCard = function(ingredients) {
                    $scope.ingredients = ingredients;
                };

                /* Constructor initialization */
                var nutrientId = $scope.extractNutrientIdFromUrl();
                $http({method: 'post', url: nutrientId})
                    .success(function(nutrient, statusCode, headers) {
                        $scope.nutrient = nutrient;
                        $scope.nutrient.moreEnergyThanRatio = parseFloat(headers('moreEnergyThanRatio'));
                        $scope.initializeMacronutrientsCard($scope.nutrient);
                        $scope.initializeEnergyCard($scope.nutrient);
                        $scope.initializeFruitsAndVegetablesCard($scope.nutrient);
                        $scope.initializeCommentsCard($scope.nutrient);
                    })
                    .error(function(data, status, headers, config) {
                        alert("ERROR" + data + " " + status);
                    });
                $http({method: 'get', url: "/nutrient/" + nutrientId + "/ingredients"})
                    .success(function(data) {
                        $scope.initializeIngredientsCard(data);
                    });
            }]);
        </script>
    </head>
    <body ng-controller="NutritionController">
        <main class="content">
            <header>{{nutrient.name}}</header>
            <div class="cards-container">
                <article class="card">
                    <div class="card-border">
                        <header>Macronutrients</header>
                        <main class="macronutrients">
                            <p>The amount of macronutrients per 100 grams.</p>
                            <section>
                                <div>Proteins: {{macronutrients.proteins | number: 2}}g</div>
                                <div>Carbs: {{macronutrients.carbs | number: 2}}g</div>
                                <div>Fats: {{macronutrients.fats | number: 2}}g</div>
                            </section>
                            <section id="macronutrientsChart"></section>
                        </main>
                    </div>
                </article>
                <article class="card">
                    <div class="card-border">
                        <header>Energy</header>
                        <main class="energy">
                            <p>The amount of energy per 100 grams.</p>
                            <section>
                                <div class="energy-value">
                                    <div>{{energy.kiloJoules | number: 0}}kJ</div>
                                    <div>{{energy.kiloCalories | number: 0}}kcal</div>
                                </div>
                            </section>
                            <section id="energyChart"></section>
                        </main>
                    </div>
                </article>
                <article class="card">
                    <div class="card-border">
                        <header>Fruits and Vegetables</header>
                        <main class="fruit-and-vegetables">
                            <p>The percentage of fruit and vegetables.</p>
                            <section id="fruitsAndVegetablesChart"></section>
                        </main>
                    </div>
                </article>
                <article class="card">
                    <div class="card-border">
                        <header>Comments</header>
                        <main class="comments">{{comments}}</main>
                    </div>
                </article>
                <article class="card">
                    <div class="card-border">
                        <header>Ingredients</header>
                        <main class="ingredients">
                            <p>The proportion of ingredients per 100g.</p>
                            <table class="ingredients-table">
                                <tr ng-repeat="ingredient in ingredients">
                                    <td>{{ingredient.nutrient.name}}</td>
                                    <td>{{ingredient.weight * 100 | number: 2}}g</td>
                                </tr>
                            </table>
                        </main>
                    </div>
                </article>
                <article class="card">
                    <div class="card-border">
                        <header>Edit</header>
                        <main>
                            <a href="/nutrient/{{nutrient.id}}/edit">Edit</a>
                        </main>
                    </div>
                </article>
                <%--<article class="card long-card">--%>
                    <%--<div class="card-border">--%>
                        <%--<header>Long card with a little text</header>--%>
                        <%--<main>--%>
                            <%--bla--%>
                        <%--</main>--%>
                    <%--</div>--%>
                <%--</article>--%>
                <%--<article class="card">--%>
                    <%--<div class="card-border">--%>
                        <%--<header>Lorem ipsum</header>--%>
                        <%--<main>--%>
                            <%--bla--%>
                        <%--</main>--%>
                    <%--</div>--%>
                <%--</article>--%>
                <%--<article class="card">--%>
                    <%--<div class="card-border">--%>
                        <%--<header>Lorem ipsum</header>--%>
                        <%--<main>--%>
                            <%--bla--%>
                        <%--</main>--%>
                    <%--</div>--%>
                <%--</article>--%>
            </div>
        </main>
    </body>
</html>
