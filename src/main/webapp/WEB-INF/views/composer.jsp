<!DOCTYPE html>

<html>
    <head>
        <title>Meal composer</title>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.7/angular.min.js"></script>
        <style type="text/css">
            @import url(http://fonts.googleapis.com/css?family=Alegreya+Sans);

            h1 {
                font-size: 5em;
                padding-left: 2em;
            }

            body {
                background-color: lightgray;
                font-family: 'Alegreya Sans', sans-serif;
                margin: 0;
            }

            article {
                height: 45em;
                overflow: hidden;
                box-sizing: border-box;
                float: left;
                margin: 0;
                padding-left: 1em;
                padding-right: 1em;
            }

            article:first-of-type {
                width: 35%;
                border-right: 0.5em dashed black;
            }

            article:last-of-type {
                width: 65%;
            }

            article > header {
                font-weight: bold;
                font-size: 2em;
            }

            .ingredient {
                padding: 0;
                float: left;
                width: 100%;
                margin: 0;
            }

            .ingredient > section:last-of-type > div {
                float: left;
                font-size: 2em;
                margin-left: 0;
                margin-top: 0.5em;
                width: 2em;
                height: 2.5em;
                background-color: lightgreen;
                padding-top: 0.7em;
                padding-left: 0;
                box-sizing: border-box;
                box-shadow: 1px 1px 1px gray;
            }

            .ingredient > section:last-of-type > input {
                padding: 0.5em;
                margin: 0.5em 0 0.5em -0.5em;
                border-width: 0;
                border-left: 0.1em dashed gray;
                box-sizing: border-box;
                box-shadow: 1px 1px 1px gray;
                height: 2.5em;
                font-family: inherit;
                width: 2.5em;
                float: left;
                font-size: 2em;
                background-color: lightgreen;
            }

            article div.drop-box {
                height: 39em;
                width: 85%;
                margin: 1em;
                background-color: white;
                box-shadow: 0.1em 0.1em 0.1em gray;
                float: right;
                overflow: hidden;
            }

            section.card {
                background-color: white;
                float: left;
                width: 20em;
                height: 3em;
                box-shadow: 0.1em 0.1em 0.1em gray;
                padding: 1em;
                margin: 1em;
            }

            .ingredient .card {
                background-color: lightgreen;
            }

            section.card > header > div {
                font-size: 1.3em;
                font-weight: bold;
            }

            section.card > header > div:first-of-type {
                width: 70%;
                float: left;
                text-overflow: ellipsis;
                overflow: hidden;
                white-space: nowrap;
            }

            section.card > header > div:last-of-type {
                /*width: 15%;*/
                color: gray;
                float: right;
            }

            section.card > div.macro-nutrients {
                clear: both;
            }

            section.card > div.macro-nutrients > div {
                color: gray;
                float: left;
                width: 33.33%;
                text-align: center;
            }

            section.card > div.macro-nutrients > div:first-of-type {
                text-align: left;
            }

            section.card > div.macro-nutrients > div:last-of-type {
                text-align: right;
            }

            .info-bar {
                box-sizing: border-box;
                padding: 1em;
                font-size: 1.5em;
                height: 2em;
            }

        </style>
        <script type="application/javascript">
            angular.module('kevinmodule', [])
                .controller('MealController', function($scope, $http) {
                    $scope.searchNutrients = function(filter) {
                        $http.get('/nutrients/get?filter=' + filter).success(function(data) {
                            $scope.nutrients = data;
                        });
                    };

                    $scope.searchNutrients("");
                    $scope.ingredients = [];

                    $scope.total = {energy: 0, proteins: 0, carbs: 0, fats: 0};

                    $scope.drag = function(event) {
                        console.info('drag');
                    };

                    $scope.drop = function(data) {
                        $scope.$apply(function() {
                            var ingredient = {
                                nutrient: $scope.nutrients[data],
                                weight: 1,

                                energy: function() {
                                    return this.nutrient.components['energy'] * this.weight;
                                },

                                proteins: function() {
                                    return this.nutrient.components['proteins'] * this.weight;
                                },

                                carbs: function() {
                                    return this.nutrient.components['carbs'] * this.weight;
                                },

                                fats: function() {
                                    return this.nutrient.components['fats'] * this.weight;
                                }
                            };
                            $scope.ingredients.push(ingredient);
                        });
                    };

                    $scope.totalEnergy = function() {
                        var total = 0;
                        angular.forEach($scope.ingredients, function(ingredient) {
                            total += ingredient.energy();
                        });
                        return total;
                    };

                    $scope.totalProteins = function() {
                        var total = 0;
                        angular.forEach($scope.ingredients, function(ingredient) {
                            total += ingredient.proteins();
                        });
                        return total;
                    };

                    $scope.totalCarbs = function() {
                        var total = 0;
                        angular.forEach($scope.ingredients, function(ingredient) {
                            total += ingredient.carbs();
                        });
                        return total;
                    };

                    $scope.totalFats = function() {
                        var total = 0;
                        angular.forEach($scope.ingredients, function(ingredient) {
                            total += ingredient.fats();
                        });
                        return total;
                    };
                })

                .directive('dropBox', function() {
                    return {
                        restrict: 'E',
                        templateUrl: '/resources/drop-box.html',
                        link: function(scope, element, attrs, controller) {
                            element.bind('drop', function(event) {
                                scope.drop(event.dataTransfer.getData("index"));
                            });

                            element.bind('dragover', function(event) {
                                return event.preventDefault();
                            });
                        }
                    };
                })

                .directive('card', function() {
                    return {
                        restrict: 'E',
                        templateUrl: '/resources/card.html',
                        link: function(scope, element, attrs, controller) {
                            element.bind('dragstart', function(event) {
                                var index = element.children().attr('data-index');
                                event.dataTransfer.setData("index", index);
                                console.info("start drag");
                            });
                        }
                    };
                })
        </script>
    </head>
    <body ng-app="kevinmodule" ng-controller="MealController">
        <h1>Meal composer</h1>
        <p>Use drag and drop from right to left to compose meals.</p>
        <article>
            <header style="text-align: right;">Selected nutrients</header>
            <div class="info-bar" style="padding-left: 4em;">
                Energy: {{totalEnergy() / 1000 | number: 0}}kJ
                Proteins: {{totalProteins() | number: 1}}g
                Carbs: {{totalCarbs() | number: 1}}g
                Fats: {{totalFats() | number: 1}}g
            </div>
            <drop-box/>
        </article>
        <article>
            <header>Available nutrients</header>
            <div class="info-bar">
                Filter: <input type="text" ng-model="filter" ng-change="searchNutrients(filter)"/>
            </div>
            <card ng-repeat="nutrient in nutrients"/>
        </article>
    </body>
</html>