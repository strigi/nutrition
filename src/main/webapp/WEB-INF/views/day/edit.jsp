<!DOCTYPE html>

<html>
    <head>
        <title>Day nutrition</title>

        <meta name="viewport" content="width=device-width, user-scalable=no"/>

        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.css"/>
        <style type="text/css">
            @import url(http://fonts.googleapis.com/css?family=Telex);

            body {
                font-family: 'Telex', sans-serif;
                background-color: rgb(90%, 90%, 90%);
                margin: 0;
            }

            #nutrient-search-container {
                color: white;
                display: flex;
                flex-direction: column;
                align-items: center;
                background-color: #428bca;
                padding: 0.5rem;
            }

            #nutrient-search-container > h1 {
                font-size: 1.25rem;
                margin-bottom: 0.25rem;
            }

            #nutrient-search-container > h2 {
                margin-bottom: 0.25rem;
                color: rgba(100%, 100%, 100%, 0.5);
            }

            #nutrient-search {
                align-self: stretch;
                display: flex;
                flex-direction: column;
                align-items: stretch;
            }

            #nutrient-search input {
                margin: 0 0 0.5rem 0;
                font-family: inherit;
                height: 1.75rem;
                font-size: 1.25rem;
                color: rgb(33.33%, 33.33%, 33.33%);
                border: 0 solid;
                border-radius: 0.20rem;
                padding: 0.2rem;
            }

            #nutrient-selector {
                display: flex;
                flex-direction: column;
                align-items: center;
                width: 100%;
            }

            #nutrient-selector > p:first-of-type {
                padding: 1.5rem 0 0.75rem 0;
                font-size: 1rem;
            }

            #nutrient-selector > p:last-of-type {
                font-size: 0.75rem;
                padding-bottom: 1.25rem;
            }

            #nutrient-selector > p {
                text-align: center;
            }

            #nutrient-selector > input {
                font-family: inherit;
                width: 4rem;
                height: 1.75rem;
                font-size: 1.25rem;
                color: rgb(33.33%, 33.33%, 33.33%);
                border: 0 solid;
                border-radius: 0.20rem;
                margin-bottom: 1.25rem;
                text-align: center;
            }

            #button-strip {
                display: flex;
            }

            #button-strip button {
                border-style: none;
                padding: 1rem;
                margin: 0 1rem 0.5rem 1rem;
                width: 5rem;
                background-color: rgba(100%, 100%, 100%, 0.25);
                color: #ffffff;
                font-family: inherit;
                font-style: italic;
                font-weight: bold;
                border-radius: 0.20rem;
            }

            #search-results {
                overflow-y: scroll;
                max-height: 10rem;
            }

            #search-results > div:first-of-type {
                border-top: 0.1rem solid rgba(100%, 100%, 100%, 0.25);
            }

            #search-results > div {
                padding: 1rem;
                overflow: hidden;
                text-overflow: ellipsis;
                white-space: nowrap;
                border-bottom: 0.1rem solid rgba(100%, 100%, 100%, 0.25);
            }

            /********************************/

            #overview-container {
                font-size: 0.67rem;
                padding: 0.5rem;
                background-color: darkgray;
            }

            #overview-container table {
                width: 100%;
            }

            #overview-details table th:first-of-type {
                text-align: right;
            }

            #overview-details table tr:last-of-type {
                border-top: 0.1rem solid black;
            }

            /********************************/

            #selected-nutrients-container {
                margin: 0.5rem;
            }

            .nutrient-card {
                background: #ffffff;
                margin-bottom: 0.5rem;
                padding: 0.5rem;
                box-shadow: 0 0.2rem 0.1rem rgba(0, 0, 0, 0.25);
                border-radius: 0.20rem;
                color: rgb(33.33%, 33.33%, 33.33%);
            }

            .nutrient-card > header {
                display: flex;
                justify-content: space-between;
            }

            .nutrient-card > header > span:first-of-type {
                white-space: nowrap;
                text-overflow: ellipsis;
                overflow: hidden;
                color: orange;
            }

            .nutrient-card > header > span:last-of-type {
                text-align: right;
                flex-shrink: 0;
                font-weight: bold;
            }

            .macronutrients {
                width: 100%;
                font-size: 0.75rem;
                margin-top: 0.5rem;
            }

            th {
                font-weight: bold;
            }

            th, td {
                text-align: center;
                padding: 0.25rem;
            }
        </style>

        <script type="application/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.js"></script>
        <script type="application/javascript" src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.14/angular.js"></script>

        <script type="application/javascript">
            var nutrition = angular.module("nutrition", []);

            nutrition.controller('NutritionController', ['$scope', '$http', '$filter', function($scope, $http) {
                var date = location.pathname.split('/').pop();

                $http.post('/day/' + date).success(function(data) {
                    $scope.day = data;
                });

                $scope.showOverviewDetails = false;

                $scope.search = function() {
                    if($scope.filter.length < 2) {
                        $scope.searchResults = [];
                        return;
                    }

                    $http.get("/nutrients/get?filter=" + $scope.filter).success(function(data) {
                        $scope.searchResults = data;
                    });
                };

                $scope.navigate = function(nutrientId) {
                    window.open("/nutrient/" + nutrientId, '_self');
                };

                $scope.clearSearch = function() {
                    $scope.searchResults = [];
                    $scope.filter = "";
                    $scope.weight = "";
                    $scope.selectedNutrient = null;
                };

                $scope.selectNutrient = function(selectedIndex) {
                    $scope.selectedNutrient = $scope.searchResults[selectedIndex];
                };

                $scope.confirmNutrient = function() {
                    var weight = parseInt($scope.weight);
                    if(!weight) {
                        $scope.weight = "";
                        return;
                    }

                    $http.get("/day/" + date + "/add?nutrientId=" + $scope.selectedNutrient.id + "&weight=" + weight).success(function(data) {
                        $scope.day = data;
                        $scope.clearSearch();
                    });
                };

                $scope.rejectNutrient = function() {
                    $scope.selectedNutrient = null;
                    $scope.clearSearch();
                };

                $scope.toggleOverviewDetails = function() {
                    $scope.showOverviewDetails = !$scope.showOverviewDetails;
                };
            }]);
        </script>
    </head>
    <body ng-app="nutrition" ng-controller="NutritionController">
        <header id="nutrient-search-container">
            <h1>Day nutrition</h1>
            <h2>{{day.date | date: 'longDate'}}</h2>
            <section id="nutrient-search" ng-if="!selectedNutrient">
                <input id="searchField" type="text" placeholder="Add nutrient" ng-change="search()" ng-model="$parent.filter"/>
                <section id="search-results">
                    <div ng-click="selectNutrient($index)" ng-repeat="nutrient in searchResults">{{nutrient.name}}</div>
                </section>
            </section>
            <section id="nutrient-selector" ng-if="selectedNutrient">
                <p>{{selectedNutrient.name}}</p>
                <p>How many grams?</p>
                <input id="weightField" type="number" min="0" ng-model="$parent.weight"/>
                <div id="button-strip">
                    <button ng-click="confirmNutrient()">Ok</button>
                    <button ng-click="rejectNutrient()">Cancel</button>
                </div>
            </section>
            <section>

            </section>
        </header>
        <section id="overview-container">
            <div id="overview-summary" ng-if="!showOverviewDetails" ng-click="toggleOverviewDetails()">
                <table>
                    <tr>
                        <th>Energy</th>
                        <th>Proteins</th>
                        <th>Carbs</th>
                        <th>Fats</th>
                    </tr>
                    <tr>
                        <td>{{day.current.energy / 1000 | number: 0}}kJ</td>
                        <td>{{day.current.proteins | number: 0}}g</td>
                        <td>{{day.current.carbs | number: 0}}g</td>
                        <td>{{day.current.fats | number: 0}}g</td>
                    </tr>
                </table>
            </div>
            <div id="overview-details" ng-if="showOverviewDetails"  ng-click="toggleOverviewDetails()">
                <table>
                    <tr>
                        <th></th>
                        <th>Energy</th>
                        <th>Proteins</th>
                        <th>Carbs</th>
                        <th>Fats</th>
                    </tr>
                    <tr>
                        <th>Target</th>
                        <td>{{day.target.energy / 1000 | number: 0}}kJ</td>
                        <td>{{day.target.proteins | number: 0}}g</td>
                        <td>{{day.target.carbs | number: 0}}g</td>
                        <td>{{day.target.fats | number: 0}}g</td>
                    </tr>
                    <tr>
                        <th>Current</th>
                        <td>{{day.current.energy / 1000 | number: 0}}kJ</td>
                        <td>{{day.current.proteins | number: 0}}g</td>
                        <td>{{day.current.carbs | number: 0}}g</td>
                        <td>{{day.current.fats | number: 0}}g</td>
                    </tr>
                    <tr>
                        <th>Remaining</th>
                        <td>{{day.remaining.energy / 1000 | number: 0}}kJ</td>
                        <td>{{day.remaining.proteins | number: 0}}g</td>
                        <td>{{day.remaining.carbs | number: 0}}g</td>
                        <td>{{day.remaining.fats | number: 0}}g</td>
                    </tr>
                </table>
            </div>
        </section>
        <section id="selected-nutrients-container">
            <article ng-click="navigate(entry.nutrient.id)" class="nutrient-card" ng-repeat="entry in day.entries">
                <header>
                    <span>{{entry.nutrient.name}}</span>
                    <span>{{entry.weight | number: 0}}g</span>
                </header>
                <table class="macronutrients">
                    <tr>
                        <th>Energy</th>
                        <th>Proteins</th>
                        <th>Carbs</th>
                        <th>Fats</th>
                    </tr>
                    <tr>
                        <td>{{entry.components.energy / 1000 | number: 0}}kJ</td>
                        <td>{{entry.components.proteins | number: 2}}g</td>
                        <td>{{entry.components.carbs | number: 2}}g</td>
                        <td>{{entry.components.fats | number: 2}}g</td>
                    </tr>
                </table>
            </article>
        </section>
    </body>
</html>