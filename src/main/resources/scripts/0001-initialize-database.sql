create table Day (
  id integer not null auto_increment,
  date date,
  primary key (id)
);

create table Day_Ingredient (
  Day_id integer not null,
  entries_id integer not null,
  entries_ORDER integer not null,
  primary key (Day_id, entries_ORDER)
);

create table Ingredient (
  id integer not null auto_increment,
  weight double precision not null,
  nutrient_id integer,
  primary key (id)
);

create table Nutrient (
  DTYPE varchar(31) not null,
  id integer not null auto_increment,
  energy double precision not null,
  name varchar(255),
  label varchar(255),
  primary key (id)
);

create table Nutrient_Ingredient (
  Nutrient_id integer not null,
  ingredients_id integer not null,
  primary key (Nutrient_id, ingredients_id)
);

create table Nutrient_measurements (
  Nutrient_id integer not null,
  measurements double precision
);

create table components (
  SimpleNutrient_id integer not null,
  components double precision,
  components_KEY varchar(255),
  primary key (SimpleNutrient_id, components_KEY)
);

alter table Day_Ingredient
add constraint UK_jstny8fqbgj028m3q4purwqg7 unique (entries_id);

alter table Nutrient_Ingredient
add constraint UK_89fr8w69fdtc5jhsmt0kcfc0x unique (ingredients_id);

alter table Day_Ingredient
add index FK_jstny8fqbgj028m3q4purwqg7 (entries_id),
add constraint FK_jstny8fqbgj028m3q4purwqg7
foreign key (entries_id)
references Ingredient (id);

alter table Day_Ingredient
add index FK_tgacjo750b510776r3pcrrs43 (Day_id),
add constraint FK_tgacjo750b510776r3pcrrs43
foreign key (Day_id)
references Day (id);

alter table Ingredient
add index FK_hogrcj82xw73cnuwjn80n6p6u (nutrient_id),
add constraint FK_hogrcj82xw73cnuwjn80n6p6u
foreign key (nutrient_id)
references Nutrient (id);

alter table Nutrient_Ingredient
add index FK_89fr8w69fdtc5jhsmt0kcfc0x (ingredients_id),
add constraint FK_89fr8w69fdtc5jhsmt0kcfc0x
foreign key (ingredients_id)
references Ingredient (id);

alter table Nutrient_Ingredient
add index FK_x18i7shlc5dbtp1hqn5ilf9x (Nutrient_id),
add constraint FK_x18i7shlc5dbtp1hqn5ilf9x
foreign key (Nutrient_id)
references Nutrient (id);

alter table Nutrient_measurements
add index FK_1jc4prvj23exnhn763yitbpi8 (Nutrient_id),
add constraint FK_1jc4prvj23exnhn763yitbpi8
foreign key (Nutrient_id)
references Nutrient (id);

alter table components
add index FK_ev6s2aujy554hv91pwld7k7no (SimpleNutrient_id),
add constraint FK_ev6s2aujy554hv91pwld7k7no
foreign key (SimpleNutrient_id)
references Nutrient (id);