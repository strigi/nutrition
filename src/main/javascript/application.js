var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var configuration = require('./configuration');
var restApi = require('./rest-api');

var application = express();

mongoose.connect(configuration.mongodb);

var baseDirectory = './src/main/javascript';

application.use(bodyParser.json());
application.use(express.static(baseDirectory + '/public'));

application.use('/api', restApi.router);


var nutrientRouter = express.Router();
application.use('/nutrient', nutrientRouter);

nutrientRouter.get('/list', function(request, response) {
    response.sendfile(baseDirectory + '/private/nutrient/list.html');
});

nutrientRouter.get('/:id', function(request, response) {
    response.sendfile(baseDirectory + '/private/nutrient/details.html');
});

var dayRouter = express.Router();
application.use('/day', dayRouter);

dayRouter.get('/list', function(request, response) {
    response.sendfile(baseDirectory + '/private/day/list.html');
});

dayRouter.get('/:date', function(request, response) {
    response.sendfile(baseDirectory + '/private/day/edit.html');
});

application.get('/statistics', function(request, response) {
    response.sendfile(baseDirectory + '/private/statistics.html');
});

application.get('/synchronization', function(request, response) {
    response.sendfile(baseDirectory + '/private/synchronization.html');
});

var server = application.listen(configuration.webserver.port, configuration.webserver.host, function() {
    console.log('HTTP Server started on %s:%d.', server.address().address, server.address().port);
});