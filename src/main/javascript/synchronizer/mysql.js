var mysql = require('mysql');
var async = require('async');

module.exports.loadNutrients = function(connectionProperties, callback) {
    console.log('Loading nutrients from MySQL');
    var connection = createConnection(connectionProperties);
    connection.query("select * from Nutrient", function(error, rows) {
        if (error) throw new Error('Unable to execute MySQL query `' + error + '`');

        var nutrients = [];
        for (var i = 0; i < rows.length; i++) {
            var row = rows[i];
            var nutrient = createNutrient(row);
            appendServings(connection, nutrient, function(nutrient) {
                switch(nutrient.migrate.type) {
                    case 'simple':
                        appendComponents(connection, nutrient, successCallback);
                        break;
                    case 'composite':
                        appendIngredients(connection, nutrient, successCallback);
                        break;
                    default: throw new Error('Unrecognized nutrient type: `' + nutrient.migrate.type + '`')
                }
                function successCallback(nutrient) {
                    registerNutrient(connection, nutrients, nutrient, rows.length, callback);
                }
            });
        }
    });
};

module.exports.loadDays = function(connectionProperties, callback) {
    var connection = createConnection(connectionProperties);
    connection.query('select * from Day', function(error, rows) {
        if(error) throw new Error('Unable to query days: ' + error);

        var days = rows.map(function(row) {
            return {
                date: row.date,
                migrate: {
                    id: row.id
                }
            };
        });

        async.eachSeries(days, appendEntries.bind(null, connection), function() {
            closeConnection(connection);
            callback(days);
        });
    });
};

function appendEntries(connection, day, callback) {
    connection.query('select * from Day_Ingredient di join Ingredient i on di.entries_id = i.id where di.Day_id = ? order by di.entries_ORDER desc', [day.migrate.id], function(error, rows) {
        if(error) throw new Error('Unable to query days: ' + error);

        if(rows.length == 0) {
            throw new Error('No entries found for day `' + day.date + '`');
        }

        day.entries = [];
        for(var i = 0; i < rows.length; i++) {
            var row = rows[i];
            day.entries.push({
                weight: row.weight,
                migrate: {
                    nutrientId: row.nutrient_id
                }
            });
        }
        callback();
    });
}

function createConnection(connectionProperties) {
    console.log('Creating MySQL connection');
    return mysql.createConnection(connectionProperties);
}

function closeConnection(connection) {
    console.log('Ending MySQL Connection');
    connection.end();
}

function createNutrient(row) {
    var nutrient = {
        name: row.name,
        comments: row.comments,
        energy: row.energy,
        migrate: {
            id: row.id,
            type: row.DTYPE == 'SimpleNutrient' ? 'simple' : 'composite'
        }
    };
    if(row.label) {
        nutrient.servings = [{
            label: row.label
        }]
    }
    return nutrient;
}

function appendComponents(connection, nutrient, callback) {
    connection.query("select * from components where SimpleNutrient_id = ?", [nutrient.migrate.id], function(error, rows) {
        if (error) throw new Error('Unable execute SQL query: `' + error + '`');

        if(rows.length != 6) {
            throw new Error('Unable to retrieve all nutrient components for `' + nutrient.name + '`components');
        }

        for (var i = 0; i < rows.length; i++) {
            var row = rows[i];
            switch(row.components_KEY) {
                case 'energy':
                    if(nutrient.energy != row.components) {
                        throw "Reduntant field energy is not the same on nutrient ("
                            + nutrient.energy + ") and components (" + row.components + ") for nutrient '" + nutrient.id + "'";
                    }
                    nutrient.energy = row.components;
                    break;
                case 'proteins': nutrient.proteins = row.components; break;
                case 'carbs': nutrient.carbs = row.components; break;
                case 'fats': nutrient.fats = row.components; break;
                case 'fruits': nutrient.fruits = row.components; break;
                case 'vegetables': nutrient.vegetables = row.components; break;
                default: throw new Error('Unexpected components key: `' + row.components_KEY + '`');
            }
        }
        callback(nutrient);
    });
}

function appendIngredients(connection, nutrient, callback) {
    connection.query("select * from Nutrient_Ingredient ni join Ingredient i on i.id = ni.ingredients_id where ni.Nutrient_id = ?", [nutrient.migrate.id], function(error, rows) {
        if(error) throw new Error('Unable to execute SQL query: `' + error + '`');

        if(rows.length == 0) {
            throw new Error('At least one ingredient was expected for nutrient `' + nutrient.name + '`')
        }

        nutrient.ingredients = [];
        for (var i = 0; i < rows.length; i++) {
            var row = rows[i];
            nutrient.ingredients.push({
                weight: row.weight,
                migrate: {
                    nutrientId: row.nutrient_id
                }
            });
        }
        callback(nutrient);
    });
}

function appendServings(connection, nutrient, callback) {
    connection.query("select * from Nutrient_measurements where Nutrient_id = ?", [nutrient.migrate.id], function(error, rows) {
        if(error) {
            throw new Error('Error while retrieving nutrient servings: ' + error);
        }
        if(rows.length == 0 && nutrient.servings) {
            throw new Error('A serving label has been found but no measurements for nutrients `' + nutrient.name + '`')
        }
        if(rows.length > 0) {
            if(!nutrient.servings) {
                throw new Error('Nutrient `' + nutrient.name + '` does not have a serving label but does have serving measurements.');
            }
            nutrient.servings[0].measurements = [];
            for (var i = 0; i < rows.length; i++) {
                nutrient.servings[0].measurements.push(rows[i].measurements);
            }
        }
        callback(nutrient);
    });
}

function registerNutrient(connection, nutrients, nutrient, size, callback) {
    nutrients.push(nutrient);
    if (nutrients.length == size) {
        closeConnection(connection);
        callback(nutrients);
    }
}