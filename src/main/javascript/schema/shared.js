exports.addVirtualIngredientReducingComponent = function(schema, componentName, ingredientArrayName) {
    schema.virtual(componentName).get(function() {
        return this[ingredientArrayName].reduce(function(previous, entry) {
            return previous + entry[componentName];
        }, 0);
    });
};

exports.arrayNotEmpty = function(array) {
    return array.length > 0;
};