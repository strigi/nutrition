var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;

var ingredientSchema = new Schema({
    weight: {type: Number, required: true, validate: [positive, 'Value for "{PATH}" must be positive.']},
    nutrient: {type: ObjectId, ref: 'nutrients', required: true}
}, {_id:  false, id: false});

addVirtualComponent(ingredientSchema, 'energy');
addVirtualComponent(ingredientSchema, 'proteins');
addVirtualComponent(ingredientSchema, 'carbs');
addVirtualComponent(ingredientSchema, 'fats');
addVirtualComponent(ingredientSchema, 'fruits');
addVirtualComponent(ingredientSchema, 'vegetables');

function addVirtualComponent(schema, componentName) {
    schema.virtual(componentName).get(function() {
        if(this.nutrient[componentName] === undefined) {
            throw new Error('Unable to resolve nutrient component `' + componentName + '` for `' + this.nutrient + '`. Did you populate this reference?');
        }
        return this.weight * this.nutrient[componentName];
    });
}

function positive(value) {
    return value > 0;
}

exports.ingredientSchema = ingredientSchema;