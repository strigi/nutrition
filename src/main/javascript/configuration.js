console.log('Starting application in `%s` mode', isProduction() ? 'production' : 'development');

function resolveWebServerConfiguration() {
    if(isProduction()) {
        return {
            host: process.env.OPENSHIFT_NODEJS_IP,
            port: process.env.OPENSHIFT_NODEJS_PORT
        };
    } else {
        return {
            host: '0.0.0.0',
            port: 8080
        };
    }
}

function resolveMongoDbConfiguration() {
    if(isProduction()) {
        return 'mongodb://admin:R3mlpYYHIp2G@' + process.env.OPENSHIFT_MONGODB_DB_HOST + ':' + process.env.OPENSHIFT_MONGODB_DB_PORT + '/nutrition';
    } else {
        return 'mongodb://localhost/nutrition';
    }
}

function resolveMySqlConfiguration() {
    return {
        host: '84.198.155.59',
        database: 'nutrition',
        user: 'nodejs-syncer',
        password: 'SWJWycGT9LDv'
    };
}

function isProduction() {
    return process.env.OPENSHIFT_NODEJS_IP !== undefined;
}

Object.defineProperties(module.exports, {
    webserver: { value: resolveWebServerConfiguration() },
    mysql: { value: resolveMySqlConfiguration() },
    mongodb: { value: resolveMongoDbConfiguration() }
});