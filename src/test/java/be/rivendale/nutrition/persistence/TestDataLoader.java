package be.rivendale.nutrition.persistence;

import be.rivendale.nutrition.domain.Ingredient;
import be.rivendale.nutrition.nutrient.CompositeNutrient;
import be.rivendale.nutrition.nutrient.Nutrient;
import be.rivendale.nutrition.nutrient.Serving;
import be.rivendale.nutrition.nutrient.SimpleNutrient;

import javax.persistence.EntityManager;

public class TestDataLoader {
    private static final int TO_BE_DETERMINED = 0;
    private EntityManager entityManager;

    public TestDataLoader(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void load() {
        persist(new SimpleNutrient("Vers - Banaan", 0.012, 0.188, 0.002, 3630, 1, 0,
                null, new Serving("Keuze mama", 91, 111, 92, 97, 94, 95, 102)));
        persist(new SimpleNutrient("Vers - Witte druiven", 0.005, 0.135, 0, 2420, 1, 0));
        persist(new SimpleNutrient("Vers - Appel", 0.004, 0.12, 0, 2290, 1, 0));
        persist(new SimpleNutrient("Vers - Peer", 0.005, 0.11, 0.003, 2290, 1, 0));
        persist(new SimpleNutrient("Vers - Sinaasappel", 0.01, 0.095, 0.002, 1860, 1, 0));
        persist(new SimpleNutrient("Vers - Mandarijn", 0.007, 0.101, 0.003, 2090, 1, 0));
        persist(new SimpleNutrient("Vers - Aardbei", 0.008, 0.065, 0.004, 1540, 1, 0));
        persist(new SimpleNutrient("Vers - Kiwi", 0.01, 0.103, 0.002, 2050, 1, 0));

        persist(new SimpleNutrient("Vers - Wortelen", 0.0093, 0.0958, 0.0024, 1725, 0, 1));                             // Source: USDA
        persist(new SimpleNutrient("Vers - Paprika: Rood", 0.0099, 0.0603, 0.003, 1297, 0, 1));                         // Source: USDA
        persist(new SimpleNutrient("Vers - Uien", 0.011, 0.0934, 0.001, 1674, 0, 1));                                   // Source: USDA
        persist(new SimpleNutrient("Ringis - Wortelschijfjes (diepvries)", 0.004, 0.065, 0, 1460, 0, 1));               // Source: product label
        persist(new SimpleNutrient("Ringis - Pompoen In Kubusblokjes (diepvries)", 0.007, 0.022, 0.002, 640, 0, 1));    // Source: product label
        persist(new SimpleNutrient("Ringis - Knolselder Blokjes (diepvries)", 0.02, 0.05, 0, 1170, 0, 1));              // Source: product label
        persist(new SimpleNutrient("Ringis - Courgette Schijfjes (diepvries)", 0.02, 0.05, 0, 1170, 0, 1));             // Source: product label
        persist(new SimpleNutrient("Ringis - Prei Blokjes (diepvries)", 0.02, 0.176, 0, 3830, 0, 1));                   // Source: product label
        persist(new SimpleNutrient("Delhaize - Prei In Roomsaus (diepvries)", 0.019, 0.05, 0.054, 3360, 0, 0.72));      // Source: product label
        persist(new SimpleNutrient("Delhaize - Extra Fijne Doperwten (diepvries)", 0.04, 0.1, 0, 2780, 0, 1));          // Source: product label
        persist(new SimpleNutrient("Elvea - Dubbel Geconcentreerde Tomatenpuree", 0.039, 0.204, 0.004, 4390, 0, 1));    // Source: product label
        persist(new CompositeNutrient("Kevin - Groenten mix", null,
                new SimpleNutrient("Vers - Ijsbergsla", 0.009, 0.0297, 0.0014, 585.76, 0, 1),                           // Source: USDA 11252, Lettuce, iceberg (includes crisphead types), raw
                nutrientByName("Vers - Paprika: Rood"),
                nutrientByName("Vers - Wortelen"),
                new SimpleNutrient("Vers - Tomaat", 0.01, 0.029, 0.002, 810, 0, 1),
                new SimpleNutrient("Vers - Komkommer", 0.006, 0.022, 0.002, 620, 0, 1)
        ));

        persist(new CompositeNutrient("Kevin - Ontbijtvlokken mix", null,
                new SimpleNutrient("Delhaize Bio - Havervlokken", 0.135, 0.587, 0.07, 15660, 0, 0),                         // Source: product label
                new SimpleNutrient("Brinta - Brinta Classic", 0.11, 0.65, 0.021, 14550, 0, 0),                              // Source: product label
                new SimpleNutrient("De Halm - Speltvlokken", 0.122, 0.693, 0.026, 14810, 0, 0),                             // Source: product label
                new SimpleNutrient("De Halm - Gerstvlokken", 0.091, 0.77, 0.02, 16170, 0, 0),                               // Source: product label
                new SimpleNutrient("De Halm - Quinoavlokken", 0.162, 0.639, 0.069, 16450, 0, 0),                            // Source: product label
                new SimpleNutrient("De Halm - Gierstvlokken", 0.099, 0.699, 0.029, 14850, 0, 0),                            // Source: product label
                new SimpleNutrient("De Halm - Boekweitvlokken", 0.103, 0.699, 0.026, 14590, 0, 0),                          // Source: product label
                new SimpleNutrient("De Halm - Rijstvlokken", 0.076, 0.78, 0.02, 15290, 0, 0)                                // Source: product label
        ));

        persist(new SimpleNutrient("Pistachenoten", 0.195, 0.24, 0.515, 27200, 0, 0));
        persist(new CompositeNutrient("Noten mix", null,
                new SimpleNutrient("Pindanoten", 0.26, 0.112, 0.52, 26120, 0, 0),
                new SimpleNutrient("Walnoten", 0.144, 0.121, 0.625, 28270, 0, 0),
                new SimpleNutrient("Amandelnoten", 0.195, 0.104, 0.52, 25480, 0, 0),
                new SimpleNutrient("Hazelnoten", 0.142, 0.106, 0.625, 27940, 0, 0),
                new SimpleNutrient("Cashewnoten", 0.185, 0.225, 0.465, 24770, 0, 0),
                new SimpleNutrient("Paranoten", 0.14, 0.045, 0.67, 28470, 0, 0),
                new SimpleNutrient("Pecannoten", 0.085, 0.095, 0.72, 30060, 0, 0)
        ));

        persist(new SimpleNutrient("Everyday - Magere Melk", 0.035, 0.049, 0.001, 1450, 0, 0));                         // Source: product label
        persist(new SimpleNutrient("Galaxi - Extra Lichte Room", 0.031, 0.058, 0.072, 4300, 0, 0));                     // Source: product label
        persist(new SimpleNutrient("Delhaize 365 - Geraspte Emmental 45+ [kaas]", 0.277, 0.02, 0.276, 15210, 0, 0));    // Source: product label

        persist(new SimpleNutrient("Water", 0, 0, 0, 0, 0, 0));                                                         // Source: USDA
        persist(new SimpleNutrient("Coca-Cola", 0, 0.106, 0, 1800, 0, 0));                                              // Source: product label

        persist(new SimpleNutrient("Cacaopoeder", 0.041, 0.81, 0.032, 16160, 0, 0, "Replace with product label (shovit alidi)", null));
        persist(new SimpleNutrient("Nestle - Nesquik", 0.05, 0.796, 0.03, 16010, 0, 0));                                // Source: product label

        persist(new SimpleNutrient("Delhaize 365 - Hagelslag Melk", 0.056, 0.74, 0.137, 18850, 0, 0));                  // Source: product label

        persist(new CompositeNutrient("Kevin - Magere Chocomelk", null,
                new Ingredient(nutrientByName("Nestle - Nesquik"), 10),
                new Ingredient(nutrientByName("Everyday - Magere Melk"), 250)
        ));

        persist(new SimpleNutrient("Generic - Hardgekookt ei", 0.1258, 0.0112, 0.1061, 6485.20, 0, 0,
                "Source: USDA 01129, Egg, whole, cooked, hard-boiled",
                new Serving("L (63-73g), gepeld", 59.7)));
        persist(new SimpleNutrient("Generic - Rauw ei", 0.1256, 0.0072, 0.0951, 5983.12, 0, 0));                        // Source: USDA 01123, Egg, whole, raw, fresh
        persist(new SimpleNutrient("Generic - Kipfilet (bereid)", 0.3, 0, 0.04, 6580, 0, 0));
        persist(new SimpleNutrient("Generic - Kipfilet (rauw)", 0.228, 0, 0.009, 4200, 0, 0));                          // Source: www.voedingswaardetabel.nl
        persist(new SimpleNutrient("Generic - Gekruid Varkens- Kalfsgehakt (rauw)", 0.14, 0, 0.245, 11560, 0, 0));      // Source: internubel
        persist(new SimpleNutrient("Ernell - Hespenworst", 0.099, 0.038, 0.313, 14000, 0, 0,
            "Source: product label",
            new Serving("Schijfje", 16))
        );

        persist(new SimpleNutrient("Kelvin - Kabeljauwhaasje", 0.16, 0, 0.01, 2940, 0, 0));                             // Source: product label
        persist(new SimpleNutrient("Kelvin - Schelvishaasje", 0.18, 0, 0.01, 3320, 0, 0));                              // Source: product label
        persist(new SimpleNutrient("Kelvin - Tropische Tongfilets", 0.17, 0, 0.01, 3260, 0, 0));                        // Source: product label
        persist(new SimpleNutrient("Kelvin - Zalmforel", 0.208, 0, 0.066, 5980, 0, 0));                                 // Source: product label
        persist(new SimpleNutrient("Kelvin - Gemarineerde Zalmtournedos", 0.196, 0.009, 0.154, 9180, 0, 0));            // Source: product label
        persist(new SimpleNutrient("Zalm", 0.2, 0, 0.135, 8390, 0, 0)); // TODO: verify and replace with Kelvin

        persist(new SimpleNutrient("Gekookte Rijst", 0.025, 0.2, 0.004, 4030, 0, 0));
        persist(new SimpleNutrient("Pasta (bereid)", 0.045, 0.175, 0.01, 4170, 0, 0));
        persist(new SimpleNutrient("Gekookte aardappelen", 0.02, 0.17, 0.001, 3460, 0, 0));                             // Behoort bij de graanproducten
        persist(new SimpleNutrient("Vers - Aardappelen", 0.0202, 0.1747, 0.0009, 3221.68, 0, 0));                        // Source: USDA
        persist(new SimpleNutrient("Lutosa - Bintje Puree", 0.02, 0.175, 0.002, 3510, 0, 0));                           // Source: product label
        persist(new SimpleNutrient("Delhaize - Lasagne Bolognaise", 0.082, 0.11, 0.082, 6380, 0, 0));                   // Source: product label

        persist(new CompositeNutrient("Lutosa - Bintje Puree (bereid, met melk)", null,
                new Ingredient(nutrientByName("Lutosa - Bintje Puree"), 500),
                new Ingredient(nutrientByName("Everyday - Magere Melk"), 133)
        ));

        persist(new SimpleNutrient("D&L - Mayonaise", 0.013, 0.016, 0.811, 30560, 0, 0));                               // Source: product label
        persist(new SimpleNutrient("Generic - Olijfolie", 0, 0.002, 0.998, 36960, 0, 0));                               // Source: www.voedingswaardetabel.nl
        persist(new SimpleNutrient("Becel - Slaolie", 0, 0, 1, 37650, 0, 0));                                           // Source: product label
        persist(new SimpleNutrient("Becel - Bakken en Braden [boter]", 0, 0, 0.92, 35000, 0, 0)); // P,C= <0.5          // Source: product label

        persist(new SimpleNutrient("Generic - Kerrie", 0.125, 0.36, 0.12, 13640, 0, 0));
        persist(new SimpleNutrient("Generic - Zout", 0, 0, 0, 0, 0, 0));                                                // Source: USDA
        persist(new SimpleNutrient("Generic - Zwarte Peper", 0.1039, 0.6395, 0.0326, 10502, 0, 0));                     // Source: USDA
        persist(new SimpleNutrient("Generic - Peterselie, vers", 0.044, 0.015, 0.002, 1430, 0, 0));                     // Source: www.voedingswaardetabel.nl
        persist(new SimpleNutrient("Generic - Knoflook", 0.06, 0.28, 0.001, 1400, 0, 0));                               // Source: www.voedingswaardetabel.nl
        persist(new SimpleNutrient("Generic - Basilicum (gedroogd)", 0.2298, 0.4775, 0.0407, 9748.72, 0, 0));           // Source: USDA 02003, Spices, basil, dried
        persist(new SimpleNutrient("Generic - Marjolein (gedroogd)", 0.1266, 0.6056, 0.0704, 11338.64, 0, 0));          // Source: USDA 02023, Spices, marjoram, dried
        persist(new SimpleNutrient("Generic - Tijm (gedroogd)", 0.0911, 0.6394, 0.0743, 11547.84, 0, 0));               // Source: USDA 02042, Spices, thyme, dried
        persist(new SimpleNutrient("Generic - Rozemarijn (gedroogd)", 0.0488, 0.6406, 0.1522, 13849.04, 0, 0));         // Source: USDA 02036, Spices, rosemary, dried
        persist(new SimpleNutrient("Generic - Salie (gedroogd)", 0.1063, 0.6073, 0.1275, 13179.60, 0, 0));              // Source: USDA 02038, Spices, sage, ground (dit is gedroogd, zie water %)
        persist(new SimpleNutrient("Generic - Bonenkruid (gedroogd)", 0.0673, 0.6873, 0.0591, 11380.48, 0, 0));         // Source: USDA 02039, Spices, savory, ground
        persist(new SimpleNutrient("Generic - Peterselie (gedroogd)", 0.2663, 0.5064, 0.0548, 12217.28, 0, 0));         // Source: USDA 02029, Spices, parsley, dried
        persist(new SimpleNutrient("Generic - Oregano (gedroogd)", 0.09, 0.6892, 0.0428, 11087.60, 0, 0));              // Source: USDA 02027, Spices, oregano, dried


        persist(new CompositeNutrient("Generic - Provençaalse Kruiden (gedroogd)",
            "This should be a pretty good estimation of a generic composition. It's based on a collection of sources: Kania ingredients, and Ducros ratios",
            new Ingredient(nutrientByName("Generic - Rozemarijn (gedroogd)"), 28),
            new Ingredient(nutrientByName("Generic - Tijm (gedroogd)"), 18),
            new Ingredient(nutrientByName("Generic - Peterselie (gedroogd)"), 10.8),
            new Ingredient(nutrientByName("Generic - Basilicum (gedroogd)"), 10.8),
            new Ingredient(nutrientByName("Generic - Marjolein (gedroogd)"), 10.8),
            new Ingredient(nutrientByName("Generic - Oregano (gedroogd)"), 10.8),
            new Ingredient(nutrientByName("Generic - Salie (gedroogd)"), 10.8)
        ));

        persist(new SimpleNutrient("Generic - Maiszetmeel", 0.003, 0.905, 0.001, 15510, 0, 0));                         // Source: www.voedingswaardetabel.nl
        persist(new SimpleNutrient("Generic - Paneermeel (ongekruid)", 0.1335, 0.7198, 0.053, 16526.8, 0, 0));          // Source: USDA 18079, Bread crumbs, dry, grated, plain

        persist(new SimpleNutrient("Generic - Wit Brood", 0.0915, 0.4906, 0.0319, 11087.6, 0, 0));                      // Source: USDA
        persist(new SimpleNutrient("Delhaize - Parijs Stokbrood", 0.076, 0.492, 0.007, 9750, 0, 0));                    // Source: product label
        persist(new SimpleNutrient("Delhaize - Ciabatta [brood]", 0.075, 0.462, 0.008, 9290, 0, 0));                    // Source: product label
        persist(new SimpleNutrient("Generic - Kaiserbroodje", 0.099, 0.527, 0.043, 12259.12, 0, 0));                    // Source: USDA 18353, Rolls, hard (includes kaiser)

        persist(new SimpleNutrient("Meel, Tarwe", 0.114, 0.622, 0.015, 13940, 0, 0));                                   // Source: www.voedingswaardetabel.nl
        persist(new SimpleNutrient("Bloem, Tarwe", 0.114, 0.698, 0.015, 14500, 0, 0));                                  // Source: www.voedingswaardetabel.nl
        persist(new SimpleNutrient("Generic - Droge gist", 0.479, 0.345, 0.042, 17000, 0, 0));                          // Source: www.voedingswaardetabel.nl
        persist(new SimpleNutrient("Suiker, wit", 0, 0.9998, 0, 16192.08, 0, 0));                                       // Source: USDA
        persist(new SimpleNutrient("Broodverbeteraar", 0.007, 0.06, 0.005, 1338.88, 0, 0));                             // Source: http://caloriecount.about.com/calories-lowan-bread-improver-i117381 (TODO: find better source)

        persist(new CompositeNutrient("Mama - Grijs Tarwebrood (50%)",
                "Get actual grams of ingredients instead of ratio fractions",
                new Ingredient(nutrientByName("Meel, Tarwe"), 0.305623472),
                new Ingredient(nutrientByName("Bloem, Tarwe"), 0.305623472),
                new Ingredient(nutrientByName("Generic - Droge gist"), 0.013447433),
                new Ingredient(nutrientByName("Suiker, wit"), 0.004889976),
                new Ingredient(nutrientByName("Generic - Zout"), 0.009779951),
                new Ingredient(nutrientByName("Broodverbeteraar"), 0.012224939),
                new Ingredient(nutrientByName("Water"), 0.348410758)
        ));

        persist(new CompositeNutrient("Mama - Wit Tarwebrood",
                "Get actual grams of ingredients instead of ratio fractions",
                new Ingredient(nutrientByName("Bloem, Tarwe"), 0.611246944),
                new Ingredient(nutrientByName("Generic - Droge gist"), 0.013447433),
                new Ingredient(nutrientByName("Suiker, wit"), 0.004889976),
                new Ingredient(nutrientByName("Generic - Zout"), 0.009779951),
                new Ingredient(nutrientByName("Broodverbeteraar"), 0.012224939),
                new Ingredient(nutrientByName("Water"), 0.348410758)
        ));

        persist(new SimpleNutrient("Lays - SuperChips Salt ‘N Pepper", 0.06, 0.53, 0.34, 23000, 0, 0));                 // Source: product label
        persist(new SimpleNutrient("LU - TUC Mini Snackies - Original", 0.078, 0.63, 0.22, 20500, 0, 0));               // Source: product label

        persist(new SimpleNutrient("Böklunder - 6 Hot Dogs (American Style)", 0.13, 0.007, 0.19, 9360, 0, 0));          // Source: product label
        persist(new SimpleNutrient("Everyday - Hamblokjes (gekookt)", 0.185, 0.011, 0.05, 5190, 0, 0));                 // Source: product label

        persist(new SimpleNutrient("XXL Nutrition - Perfect Whey Protein", 0.785, 0.074, 0.063, 16750, 0, 0));          // Source: product label

        persist(new SimpleNutrient("Knorr - Bouillon Keteltje: Kip", 0.001, 0.003, 0.004, 200, 0, 0,
                "Source: product label (100ml)",
                new Serving("Keteltje", 28)));

        persist(new SimpleNutrient("Boursin - Cuisine: Tomaat & Mediterrane Kruiden", 0.03, 0.035, 0.135, 6100,
            0, TO_BE_DETERMINED, "Source: product label. Still need to determine vegetable ratio!", null)
        );

        persist(new CompositeNutrient("Mama - Pompoensoep", null,
                new Ingredient(nutrientByName("Ringis - Pompoen In Kubusblokjes (diepvries)"), 400),
                new Ingredient(nutrientByName("Ringis - Wortelschijfjes (diepvries)"), 340),
                new Ingredient(nutrientByName("Vers - Paprika: Rood"), 340),
                new Ingredient(nutrientByName("Vers - Uien"), 270),
                new Ingredient(nutrientByName("Vers - Aardappelen"), 250),
                new Ingredient(nutrientByName("Water"), 2700),
                new Ingredient(nutrientByName("Knorr - Bouillon Keteltje: Kip"), 3*28),
                new Ingredient(nutrientByName("Boursin - Cuisine: Tomaat & Mediterrane Kruiden"), 245),
                new Ingredient(nutrientByName("Generic - Zwarte Peper"), 0),
                new Ingredient(nutrientByName("Generic - Zout"), 5)
        ));

        persist(new CompositeNutrient("Mama - Gentse Waterzooi", null,
                new Ingredient(nutrientByName("Ringis - Prei Blokjes (diepvries)"), 250),
                new Ingredient(nutrientByName("Ringis - Wortelschijfjes (diepvries)"), 250),
                new Ingredient(nutrientByName("Vers - Uien"), 250),
                new Ingredient(nutrientByName("Vers - Aardappelen"), 500),
                new Ingredient(nutrientByName("Generic - Kipfilet (rauw)"), 500),
                new Ingredient(nutrientByName("Galaxi - Extra Lichte Room"), 100),
                new Ingredient(nutrientByName("Knorr - Bouillon Keteltje: Kip"), 2*28),
                new Ingredient(nutrientByName("Generic - Maiszetmeel"), 20),
                new Ingredient(nutrientByName("Becel - Bakken en Braden [boter]"), 5), // Schatting
                new Ingredient(nutrientByName("Generic - Olijfolie"), 5), // Schatting
                new Ingredient(nutrientByName("Generic - Peterselie, vers"), 10),
                new Ingredient(nutrientByName("Water"), 750)
        ));

        persist(new CompositeNutrient("Mama - Nasi Goreng", "Should still add ingredient tabasco < 1g",
            new Ingredient(nutrientByName("Gekookte Rijst"), 500),
            new Ingredient(nutrientByName("Everyday - Hamblokjes (gekookt)"), 200),
            new Ingredient(nutrientByName("Generic - Kipfilet (rauw)"), 200),
            new Ingredient(nutrientByName("Ringis - Wortelschijfjes (diepvries)"), 300),
            new Ingredient(nutrientByName("Delhaize - Extra Fijne Doperwten (diepvries)"), 200),
            new Ingredient(nutrientByName("Vers - Uien"), 100),
            new Ingredient(nutrientByName("Generic - Knoflook"), 10),
            new Ingredient(nutrientByName("Becel - Slaolie"), 30),
            new Ingredient(nutrientByName("Generic - Zout"), 0),
            new Ingredient(nutrientByName("Generic - Zwarte peper"), 0)
        ));

        persist(new CompositeNutrient("Mama - Provençaalse Hamburger", null,
            new Ingredient(nutrientByName("Generic - Gekruid Varkens- Kalfsgehakt (rauw)"), 500),
            new Ingredient(nutrientByName("Delhaize 365 - Geraspte Emmental 45+ [kaas]"), 50),
            new Ingredient(nutrientByName("Generic - Rauw ei"), 55),
            new Ingredient(nutrientByName("Generic - Provençaalse Kruiden (gedroogd)"), 4),
            new Ingredient(nutrientByName("Generic - Paneermeel (ongekruid)"), 20)
        ));
    }

    private Nutrient nutrientByName(String name) {
        return PersistenceTestUtilities.nutrientByName(entityManager, name);
    }

    private void persist(Nutrient nutrient) {
        PersistenceTestUtilities.persist(entityManager, nutrient);
        entityManager.flush();
    }
}
