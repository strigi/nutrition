package be.rivendale.nutrition.nutrient;

import be.rivendale.nutrition.BaseTest;
import be.rivendale.nutrition.domain.Ingredient;
import org.junit.Test;

import java.util.Arrays;

import static be.rivendale.nutrition.DomainFactory.proteinSource;

public class NutrientComponentTest extends BaseTest {
    @Test
    public void testSumCountsComponentOfAllIngredients() throws Exception {
        double sum = NutrientComponent.proteins.sum(Arrays.asList(
                new Ingredient(proteinSource(0.012), 50),
                new Ingredient(proteinSource(0.2321), 75),
                new Ingredient(proteinSource(0.0912), 80)
        ));
        assertEquals(25.3035, sum);
    }
}
