package be.rivendale.nutrition.nutrient;

import be.rivendale.nutrition.BaseTest;
import be.rivendale.nutrition.domain.NutrientException;
import org.junit.Test;

import static be.rivendale.nutrition.DomainFactory.DUMMY_NUTRIENT_NAME;
import static be.rivendale.nutrition.DomainFactory.dummyNutrient;
import static be.rivendale.nutrition.nutrient.NutrientComponent.*;
import static org.junit.Assert.assertFalse;

public class SimpleNutrientTest extends BaseTest {
    @Test
    public void testEqualsReturnsTrueOfBothAreEqual() throws Exception {
        SimpleNutrient a = dummyNutrient();
        SimpleNutrient b = dummyNutrient();
        assertTrue(a.equals(b));
    }

    @Test
    public void testEqualsReturnsTrueOfBothAreSame() throws Exception {
        SimpleNutrient nutrient = dummyNutrient();
        assertTrue(nutrient.equals(nutrient));
    }

    @Test
    public void testHashCodeIsTheSameForEqualValues() throws Exception {
        SimpleNutrient a = dummyNutrient();
        SimpleNutrient b = dummyNutrient();
        assertTrue(a.hashCode() == b.hashCode());
    }

    @Test
    public void testEnergyIsIncludedInEqualsCalculation() throws Exception {
        SimpleNutrient a = new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1, 2, 3, 4, 5, 6);
        SimpleNutrient b = new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1, 2, 3, 40, 5, 6);
        assertFalse(a.equals(b));
    }

    @Test
    public void testProteinsIsIncludedInEqualsCalculation() throws Exception {
        SimpleNutrient a = new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1, 2, 3, 4, 5, 6);
        SimpleNutrient b = new SimpleNutrient(DUMMY_NUTRIENT_NAME, 10, 2, 3, 4, 5, 6);
        assertFalse(a.equals(b));
    }

    @Test
    public void testCarbsIsIncludedInEqualsCalculation() throws Exception {
        SimpleNutrient a = new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1, 2, 3, 4, 5, 6);
        SimpleNutrient b = new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1, 20, 3, 4, 5, 6);
        assertFalse(a.equals(b));
    }

    @Test
    public void testFatsIsIncludedInEqualsCalculation() throws Exception {
        SimpleNutrient a = new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1, 2, 3, 4, 5, 6);
        SimpleNutrient b = new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1, 2, 30, 4, 5, 6);
        assertFalse(a.equals(b));
    }

    @Test
    public void testFruitsIsIncludedInEqualsCalculation() throws Exception {
        SimpleNutrient a = new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1, 2, 3, 4, 5, 6);
        SimpleNutrient b = new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1, 2, 3, 4, 50, 6);
        assertFalse(a.equals(b));
    }

    @Test
    public void testVegetablesIsIncludedInEqualsCalculation() throws Exception {
        SimpleNutrient a = new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1, 2, 3, 4, 5, 6);
        SimpleNutrient b = new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1, 2, 3, 4, 5, 60);
        assertFalse(a.equals(b));
    }

    @Test
    public void testHashCodeIsBasedOnEnergy() throws Exception {
        SimpleNutrient a = new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1, 2, 3, 4, 5, 6);
        SimpleNutrient b = new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1, 2, 3, 40, 5, 6);
        assertFalse(a.hashCode() == b.hashCode());
    }

    @Test
    public void testHashCodeIsBasedOnProteins() throws Exception {
        SimpleNutrient a = new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1, 2, 3, 4, 5, 6);
        SimpleNutrient b = new SimpleNutrient(DUMMY_NUTRIENT_NAME, 10, 2, 3, 4, 5, 6);
        assertFalse(a.hashCode() == b.hashCode());
    }

    @Test
    public void testHashCodeIsBasedOnCarbs() throws Exception {
        SimpleNutrient a = new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1, 2, 3, 4, 5, 6);
        SimpleNutrient b = new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1, 20, 3, 4, 5, 6);
        assertFalse(a.hashCode() == b.hashCode());
    }

    @Test
    public void testHashCodeIsBasedOnFats() throws Exception {
        SimpleNutrient a = new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1, 2, 3, 4, 5, 6);
        SimpleNutrient b = new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1, 2, 30, 4, 5, 6);
        assertFalse(a.hashCode() == b.hashCode());
    }

    @Test
    public void testFruitsIsIncludedInHashCodeCalculation() throws Exception {
        SimpleNutrient a = new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1, 2, 3, 4, 5, 6);
        SimpleNutrient b = new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1, 2, 3, 4, 50, 6);
        assertFalse(a.hashCode() == b.hashCode());
    }

    @Test
    public void testVegetablesIsIncludedInHashCodeCalculation() throws Exception {
        SimpleNutrient a = new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1, 2, 3, 4, 5, 6);
        SimpleNutrient b = new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1, 2, 3, 4, 5, 60);
        assertFalse(a.hashCode() == b.hashCode());
    }

    @Test
    public void testEnergyCanBeRetrieved() throws Exception {
        assertDoubleEquals(4, dummyNutrient().getEnergy());
        assertDoubleEquals(4, dummyNutrient().getComponents().get(energy));
    }

    @Test
    public void testProteinsCanBeRetrieved() throws Exception {
        assertDoubleEquals(1, dummyNutrient().getComponents().get(proteins));
    }

    @Test
    public void testFatsCanBeRetrieved() throws Exception {
        assertDoubleEquals(3, dummyNutrient().getComponents().get(fats));
    }

    @Test
    public void testFruitsCanBeRetrieved() throws Exception {
        assertDoubleEquals(5, dummyNutrient().getComponents().get(fruits));
    }

    @Test
    public void testVegetablesCanBeRetrieved() throws Exception {
        assertDoubleEquals(6, dummyNutrient().getComponents().get(vegetables));
    }

    @Test
    public void testCarbsCanBeRetrieved() throws Exception {
        assertDoubleEquals(2, dummyNutrient().getComponents().get(carbs));
    }

    @Test
    public void testNutrientNameMustNotBeEmpty() throws Exception {
        expectedException(NutrientException.class, "Nutrient name is mandatory");
        new SimpleNutrient("", 1, 2, 3, 4, 5, 6);
    }

    @Test
    public void testNutrientNameMustNotBeBlank() throws Exception {
        expectedException(NutrientException.class, "Nutrient name is mandatory");
        new SimpleNutrient(" ", 1, 2, 3, 4, 5, 6);
    }

    @Test
    public void testNutrientNameMustNotBeNull() throws Exception {
        expectedException(NutrientException.class, "Nutrient name is mandatory");
        new SimpleNutrient(null, 1, 2, 3, 4, 5, 6);
    }

    @Test
    public void testCommentsAreSet() throws Exception {
        SimpleNutrient nutrient = new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, "This is a comment.", null);
        assertEquals("This is a comment.", nutrient.getComments());
    }
}
