package be.rivendale.nutrition;


import org.junit.Assert;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * Base test for grouping reusable features.
 */
public abstract class BaseTest {
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    /**
     * Sets up a rule to expec an exception of a specific type and with a specific message.
     * Must be called BEFORE the actual exception is thrown.
     * @param exceptionClass The expected type of exception to be thrown.
     * @param errorMessage The expected error message for the thrown excpetion.
     */
    protected void expectedException(Class<? extends Throwable> exceptionClass, String errorMessage) {
        exceptionRule.expect(exceptionClass);
        exceptionRule.expectMessage(errorMessage);
    }

    protected Date newDate(int year, Month month, int dayOfMonth) {
        return Date.from(LocalDate.of(year, month, dayOfMonth).atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    protected Date newDateTime(int year, int month, int day, int hour, int minute, int second) {
        return Date.from(ZonedDateTime.of(year, month, day, hour, minute, second, 0, ZoneId.systemDefault()).toInstant());
    }

    protected void assertDoubleEquals(double expected, double actual) {
        if(!MathUtilities.doubleEquals(expected, actual)) {
            fail(String.format("Value expected '%f' but was '%f'", expected, actual));
        }
    }

    protected void assertNotNull(Object object) {
        Assert.assertNotNull(object);
    }

    protected void assertEquals(Object expected, Object actual) {
        Assert.assertEquals(expected, actual);
    }

    protected void fail() {
        Assert.fail();
    }

    protected void fail(String message) {
        Assert.fail(message);
    }

    protected void assertTrue(boolean condition) {
        Assert.assertTrue(condition);
    }
}
