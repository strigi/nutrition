package be.rivendale.nutrition.domain;

import be.rivendale.nutrition.day.Day;
import be.rivendale.nutrition.nutrient.Nutrient;
import be.rivendale.nutrition.persistence.PersistenceTest;
import org.junit.Test;

import java.util.Date;

public class DayPersistenceTest extends PersistenceTest {
    @Test
    public void testDayCanBePersisted() throws Exception {
        Day day = persistFlushClearAndRefetch(new Day(anyDate()));
        assertNotNull(day.getId());
    }

    @Test
    public void testEntriesKeepReverseOrderOfInsertion() throws Exception {
        Day day = newDay();
        day.addEntry(new Ingredient(nutrientByName("De Halm - Gierstvlokken"), 474));
        day.addEntry(new Ingredient(nutrientByName("Vers - Wortelen"), 654));
        day.addEntry(new Ingredient(nutrientByName("Vers - Banaan"), 15));
        Day persistedDay = persistFlushClearAndRefetch(day);

        assertDoubleEquals(15, persistedDay.getEntries().get(0).getWeight());
        assertDoubleEquals(654, persistedDay.getEntries().get(1).getWeight());
        assertDoubleEquals(474, persistedDay.getEntries().get(2).getWeight());
    }

    @Test
    public void testDayCascadesPersistToIngredients() throws Exception {
        Day day = newDay();
        day.addEntry(new Ingredient(anyNutrient(), 250));
        persistFlushClearAndRefetch(day);
        assertEquals(1, day.getEntries().size());
    }

    private Day newDay() {
        return new Day(new Date());
    }

    private Nutrient anyNutrient() {
        return query("select n from Nutrient n where n.name='Vers - Banaan'", Nutrient.class);
    }

    @Test
    public void testEntriesArePersisted() throws Exception {
        Day transientDay = new Day(anyDate());
        transientDay.addEntry(new Ingredient(anyNutrient(), 250));
        Day persistentDay = persistFlushClearAndRefetch(transientDay);
        assertDoubleEquals(250, persistentDay.getEntries().get(0).getWeight());
    }

    private Date anyDate() {
        return newDateTime(1983, 7, 15, 16, 17, 18);
    }
}
