package be.rivendale.nutrition.domain;

import be.rivendale.nutrition.BaseTest;
import be.rivendale.nutrition.day.Day;
import be.rivendale.nutrition.nutrient.NutrientComponent;
import be.rivendale.nutrition.nutrient.SimpleNutrient;
import org.junit.Test;

import java.time.Month;
import java.util.Date;

public class DayTest extends BaseTest {
    private static final Ingredient INGREDIENT_A = new Ingredient(new SimpleNutrient("dummy-nutrient-a", 1, 2, 3, 4, 5, 6), 150);
    private static final Ingredient INGREDIENT_B = new Ingredient(new SimpleNutrient("dummy-nutrient-b", 7, 8, 9, 10, 11, 12), 75);

    private Date dummyDate() {
        return newDate(2014, Month.MARCH, 30);
    }

    @Test
    public void testRemainingCalculatesTheDifferenceBetweenTargetAndCurrent() throws Exception {
        Day day = newDay();
        day.addEntry(INGREDIENT_A);
        day.addEntry(INGREDIENT_B);

        for (NutrientComponent component : NutrientComponent.values()) {
            assertDoubleEquals(day.getRemaining().get(component), day.getTarget().get(component) - day.getCurrent().get(component));
        }
    }

    private Day newDay() {
        return new Day(dummyDate());
    }

    @Test
    public void testCurrentCalculatesTheSomeOfAllEntries() throws Exception {
        Day day = newDay();
        day.addEntry(INGREDIENT_A);
        day.addEntry(INGREDIENT_B);

        assertDoubleEquals(675, day.getCurrent().get(NutrientComponent.proteins));
        assertDoubleEquals(900, day.getCurrent().get(NutrientComponent.carbs));
        assertDoubleEquals(1125, day.getCurrent().get(NutrientComponent.fats));
        assertDoubleEquals(1350, day.getCurrent().get(NutrientComponent.energy));
        assertDoubleEquals(1575, day.getCurrent().get(NutrientComponent.fruits));
        assertDoubleEquals(1800, day.getCurrent().get(NutrientComponent.vegetables));
    }

    @Test
    public void testNewDayIsInitializedWithEmptyListOfEntries() throws Exception {
        Day day = newDay();
        assertTrue(day.getEntries().isEmpty());
    }

    @Test
    public void testNewDayIsInitializedWithSpecifiedDate() throws Exception {
        assertEquals(dummyDate(), new Day(dummyDate()).getDate());
    }

    @Test
    public void testAddEntryAddsAnEntryToTheTop() throws Exception {
        Day day = newDay();
        addEntryAndAssertIsFirst(day, INGREDIENT_A);
        addEntryAndAssertIsFirst(day, INGREDIENT_B);
    }

    private void addEntryAndAssertIsFirst(Day day, Ingredient ingredient) {
        day.addEntry(ingredient);
        assertEquals(ingredient.getNutrient().getName(), day.getEntries().get(0).getNutrient().getName());
    }

}
