package be.rivendale.nutrition.domain;

import be.rivendale.nutrition.BaseTest;
import be.rivendale.nutrition.nutrient.SimpleNutrient;
import org.junit.Test;

import static be.rivendale.nutrition.nutrient.NutrientComponent.*;

public class IngredientTest extends BaseTest {
    public static final SimpleNutrient DUMMY_NUTRIENT = new SimpleNutrient("dummy-nutrient", 2.0, 2.5, 3.0, 3.5, 4.0, 4.5);

    @Test
    public void testWeightCenBeRetrieved() throws Exception {
        assertDoubleEquals(0.157, new Ingredient(DUMMY_NUTRIENT, 0.157).getWeight());
    }

    @Test(expected = NullPointerException.class)
    public void testRatioThrowsIllegalArgumentExceptionIfNutrientIsNull() throws Exception {
        new Ingredient(null, 1.0);
    }

    @Test
    public void testProteinsAreMultipliedWithValue() throws Exception {
        assertDoubleEquals(0.5, new Ingredient(DUMMY_NUTRIENT, 0.25).getComponents().get(proteins));
    }

    @Test
    public void testCarbsAreMultipliedWithValue() throws Exception {
        assertDoubleEquals(0.625, new Ingredient(DUMMY_NUTRIENT, 0.25).getComponents().get(carbs));
    }

    @Test
    public void testFatsAreMultipliedWithValue() throws Exception {
        assertDoubleEquals(0.75, new Ingredient(DUMMY_NUTRIENT, 0.25).getComponents().get(fats));
    }

    @Test
    public void testEnergyIsMultipliedWithValue() throws Exception {
        assertDoubleEquals(0.875, new Ingredient(DUMMY_NUTRIENT, 0.25).getComponents().get(energy));
    }

    @Test
    public void testFruitsIsMultipliedWithValue() throws Exception {
        assertDoubleEquals(1, new Ingredient(DUMMY_NUTRIENT, 0.25).getComponents().get(fruits));
    }

    @Test
    public void testVegetablesIsMultipliedWithValue() throws Exception {
        assertDoubleEquals(1.125, new Ingredient(DUMMY_NUTRIENT, 0.25).getComponents().get(vegetables));
    }
}
